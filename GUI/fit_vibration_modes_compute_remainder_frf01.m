%% fit noticeable modes  

% allocate arrays %
kmin = zeros(Nmode,1);    kmax = zeros(Nmode,1);                            % data selection index
wn = zeros(Nmode,1);      zeta = zeros(Nmode,1);                            % wn and zeta
a = zeros(Nmode,1);       b = zeros(Nmode,1);                               % residual terms
c = zeros(Nmode,1);       d = zeros(Nmode,1);           
alpha_k = zeros(Nmode,1); beta_k  = zeros(Nmode,1);                         % residues (i.e. modal constants, mode participation factors)

for kmode = 1:Nmode
    
    % record indices and display selection data %
    % ========================================= %
    kmin(kmode) = ceil(interp1(f,(1:N)',fmin(kmode)));
    kmax(kmode) = floor(interp1(f,(1:N)',fmax(kmode)));
    
    data_range = (kmin(kmode):kmax(kmode))';
    
    if flag_show_figures
    
        figure(5);
        subplot(2,1,1); loglog(f(data_range),abs(Hdelay_removed(data_range)),'b');
        tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
        subplot(2,1,2); semilogx(f(data_range),r2d*unwrap(angle(Hdelay_removed(data_range))),'b');
        tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(6);
        subplot(2,1,1); plot(f(data_range),abs(Hdelay_removed(data_range)),'b');
        tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
        subplot(2,1,2); plot(f(data_range),r2d*unwrap(angle(Hdelay_removed(data_range))),'b');
        tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(7);
        subplot(2,1,1); semilogx(f(data_range),real(Hdelay_removed(data_range)),'b');
        tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        subplot(2,1,2); semilogx(f(data_range),imag(Hdelay_removed(data_range)),'b');
        tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        
        figure(8);
        plot(real(Hdelay_removed(data_range)), imag(Hdelay_removed(data_range)), 'b');
        axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
    
    end
    
    % fit modal parameters %
    % ==================== %
    
    % prepare arrays for convenience %
    Nrange = length(data_range);                                            % number of samples in selected range
    
    f1 = f(data_range);
    w1 = w(data_range); w2 = w1.*w1; w3 = w1.*w2;                           % positive and negative powers of frequency
    inv_w1 = 1./w1;     inv_w2 = 1./w2;

    g = real(Hdelay_removed(data_range));	h = imag(Hdelay_removed(data_range));                    % real and imaginary components of 'measured' FRF
    One  = ones(Nrange,1);      Zero = zeros(Nrange,1);                     % arrays of ones and zeros (for constructing the regressor matrix)
    
    % initial fit; only used to estimate wn and zeta values %
    Phi = [inv_w2.*g , -inv_w2 ,    Zero , -inv_w1.*h ,    Zero , inv_w1 ,  One , Zero , -inv_w2 ,    Zero ; ...
           inv_w2.*h ,    Zero , -inv_w2 ,  inv_w1.*g , -inv_w1 ,   Zero , Zero ,  One ,    Zero , -inv_w1 ];
    Y = [g ; h];
    
    theta = pinv(Phi)*Y;
    wn(kmode) = real(sqrt(theta(1)));
    zeta(kmode) = theta(4)/(2*wn(kmode));

    % after having approximately identified wn and zeta          % 
    % re-fit residual terms and residues (i.e., real and complex %
    % participation factors for the mode being identified        %
    % 2nd order response for term representing the mode
    % G2 = 1 / (s2 + 2*z*wn*s + wn^2) %
    g2 = (wn(kmode)*wn(kmode) - w2) ./ ((wn(kmode).*wn(kmode) - w2).*(wn(kmode).*wn(kmode) - w2) + (2*zeta(kmode)*wn(kmode)*w1).*(2*zeta(kmode)*wn(kmode)*w1));
    h2 = (-2*zeta(kmode)*wn(kmode)*w1) ./ ((wn(kmode).*wn(kmode) - w2).*(wn(kmode).*wn(kmode) - w2) + (2*zeta(kmode)*wn(kmode)*w1).*(2*zeta(kmode)*wn(kmode)*w1));

    Phi = [ - inv_w2 ,   Zero  ,  One , Zero , g2 , -w1.*h2 ; ...
                Zero , -inv_w1 , Zero ,   w1 , h2 ,  w1.*g2 ];
    Y = [g ; h];
    theta = pinv(Phi)*Y;

    % extract identified parameters %
    a(kmode)       = theta(1); b(kmode)      = theta(2); c(kmode) = theta(3); d(kmode) = theta(4);                 % residual terms
    alpha_k(kmode) = theta(5); beta_k(kmode) = theta(6);                                  % mode participation factors (residues)

    % contruct residual component, the mode, and their combination %
    residual_f = (-inv_w2)*a(kmode) + (-j*inv_w1)*b(kmode) + c(kmode)*One + (j*w1)*d(kmode);
    mode_f     = (alpha_k(kmode)*One + j*w1*beta_k(kmode)).*(g2+j*h2);
    total_f = residual_f + mode_f;

    if flag_show_figures
    
        % show captured response %
        figure(5);
        subplot(2,1,1); loglog(f1,abs(residual_f),'c');
        subplot(2,1,2); semilogx(f1,r2d*unwrap(angle(residual_f)),'c');
        subplot(2,1,1); loglog(f1,abs(mode_f),'g');
        subplot(2,1,2); semilogx(f1,r2d*unwrap(angle(mode_f)),'g');
        subplot(2,1,1); loglog(f1,abs(total_f),'r');
        tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
        subplot(2,1,2); semilogx(f1,r2d*unwrap(angle(total_f)),'r');
        tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(6);
        subplot(2,1,1); plot(f1,abs(residual_f),'c');
        subplot(2,1,2); plot(f1,r2d*unwrap(angle(residual_f)),'c');
        subplot(2,1,1); plot(f1,abs(mode_f),'g');
        subplot(2,1,2); plot(f1,r2d*unwrap(angle(mode_f)),'g');
        subplot(2,1,1); plot(f1,abs(total_f),'r');
        tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
        subplot(2,1,2); plot(f1,r2d*unwrap(angle(total_f)),'r');
        tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(7);
        subplot(2,1,1); semilogx(f1, real(residual_f), 'c');
        subplot(2,1,2); semilogx(f1, imag(residual_f), 'c');
        subplot(2,1,1); semilogx(f1, real(mode_f), 'g');
        subplot(2,1,2); semilogx(f1, imag(mode_f), 'g');
        subplot(2,1,1); semilogx(f1, real(total_f), 'r');
        tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        subplot(2,1,2); semilogx(f1, imag(total_f), 'r');
        tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        
        figure(8);
        plot(real(residual_f), imag(residual_f), 'c', real(mode_f), imag(mode_f), 'g', real(total_f), imag(total_f), 'r');
    end
    
    % pause;
    
end

% reconstruct the contribution of the identified modes %
mode_FRF = zeros(length(w),Nmode);
total_modeled_modes_FRF = zeros(length(w),1);

for kmode = 1:Nmode
    
    g2 = (wn(kmode)*wn(kmode) - w.*w) ./ ((wn(kmode).*wn(kmode) - w.*w).*(wn(kmode).*wn(kmode) - w.*w) + (2*zeta(kmode)*wn(kmode)*w).*(2*zeta(kmode)*wn(kmode)*w));
    h2 = (-2*zeta(kmode)*wn(kmode)*w) ./ ((wn(kmode).*wn(kmode) - w.*w).*(wn(kmode).*wn(kmode) - w.*w) + (2*zeta(kmode)*wn(kmode)*w).*(2*zeta(kmode)*wn(kmode)*w));
       
    mode_FRF(:,kmode) = (alpha_k(kmode) + j*w*beta_k(kmode)).*(g2 + j*h2);
    
    % add the contribution of the current mode to the 'modeled' FRF %
    total_modeled_modes_FRF = total_modeled_modes_FRF + mode_FRF(:,kmode);

    if flag_show_figures
        
        figure(5);
        subplot(2,1,1); loglog(f,abs(mode_FRF(:,kmode)),'g:');
        tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
        subplot(2,1,2); semilogx(f,r2d*unwrap(angle(mode_FRF(:,kmode))),'g:');
        tmp = axis; axis([fview_min fview_max  -1000 200]);
        
        figure(6);
        subplot(2,1,1); plot(f,abs(mode_FRF(:,kmode)),'g:');
        tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
        subplot(2,1,2); plot(f,r2d*unwrap(angle(mode_FRF(:,kmode))),'g:');
        tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(7);
        subplot(2,1,1); semilogx(f, real(mode_FRF(:,kmode)), 'g:');
        tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        subplot(2,1,2); semilogx(f, imag(mode_FRF(:,kmode)), 'g:');
        tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        
        figure(8);
        plot(real(mode_FRF(:,kmode)), imag(mode_FRF(:,kmode)), 'g:');
        axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);

    end

    % pause;
end


% if flag_show_figures,
%     
%     figure(9); clf; zoom on;
%     subplot(2,1,1); loglog(f,abs(Hdelay_removed),'k',w,abs(total_modeled_modes_FRF),'b.');
%     tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
%     subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hdelay_removed)),'k', w,r2d*unwrap(angle(total_modeled_modes_FRF)),'b.');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
%     
%     figure(10); clf; zoom on;
%     subplot(2,1,1); plot(f,abs(Hdelay_removed),'k',w,abs(total_modeled_modes_FRF),'b.');
%     tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
%     subplot(2,1,2); plot(f,r2d*unwrap(angle(Hdelay_removed)),'k', w,r2d*unwrap(angle(total_modeled_modes_FRF)),'b.');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
%     
%     figure(11); clf; zoom on;
%     subplot(2,1,1); semilogx(f, real(Hdelay_removed), 'k', w, real(total_modeled_modes_FRF), 'b.');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
%     subplot(2,1,2); semilogx(f, imag(Hdelay_removed), 'k', w, imag(total_modeled_modes_FRF), 'b.');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
%     
%     figure(12); clf; zoom on;
%     plot(real(Hdelay_removed), imag(Hdelay_removed), 'k', real(total_modeled_modes_FRF), imag(total_modeled_modes_FRF), 'b.');
%     axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
% 
% end

% investigate the discrepancy (remainder) terms between the 'measured' FRF 
% and the fitted modes 

Hrem = Hdelay_removed - total_modeled_modes_FRF;

if flag_show_figures

    figure(9); clf; zoom on;
    subplot(2,1,1); loglog(f,abs(Hdelay_removed),'k'); hold on;
    subplot(2,1,1); loglog(f,abs(total_modeled_modes_FRF),'b');
    subplot(2,1,1); loglog(f,abs(Hrem),'r');
    legend('measurement with assumed delay removed','fitted modes','remainder term');
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    
    ylabel('mag [m/N]');
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hdelay_removed)),'k'); hold on;
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(total_modeled_modes_FRF)),'b');
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hrem)),'r');
    tmp = axis; axis([fview_min fview_max -1000 200]);
    
    figure(11); clf; zoom on;
    subplot(2,1,1); semilogx(f,real(Hdelay_removed),'k'); hold on;
    ylabel('real [m/N]');
    subplot(2,1,1); semilogx(f, real(total_modeled_modes_FRF), 'b');
    subplot(2,1,1); semilogx(f, real(Hrem), 'r');
    legend('measurement with assumed delay removed','fitted modes','remainder term');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    
    subplot(2,1,2); semilogx(f,imag(Hdelay_removed),'k'); hold on;
    ylabel('imag [m/N]'); xlabel('freq [Hz]');
    subplot(2,1,2); semilogx(f, imag(total_modeled_modes_FRF), 'b');
    subplot(2,1,2); semilogx(f, imag(Hrem), 'r');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);

end