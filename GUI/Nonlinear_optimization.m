% Load all the data from workspace to use appdesigner
show_final_figures = evalin('base','show_final_figures');

%% analyze one chosen case and 
%  apply nonlinear optimization for further tune the model delay and
%  pole locations

load(evalin('base','fitting_results'));
log_smallest_RMS_error = min(min(min(log10(fitting_RMS_error_array))));
log_largest_RMS_error  = max(max(max(log10(fitting_RMS_error_array))));

% nonlinear optimization options in FRF fitting     %
% for adjusting the pole locations and delay amount %
optOptions=optimset;
optOptions=optimset(optOptions,'Diagnostics','on');
optOptions=optimset(optOptions,'Display','iter');                      % off | iter | final
% optOptions=optimset(optOptions,'Display','off');                     % off | iter | final
% optOptions=optimset(optOptions,'Hessian','off');
% optOptions=optimset(optOptions,'HessPattern',H);
% optOptions=optimset(optOptions,'GradObj','on');                      % analytically supply the objective function gradient
% optOptions=optimset(optOptions,'GradCons','on');                     % analytically supply the objective function gradient
% optOptions=optimset(optOptions,'DerivativeCheck','off');             % check if user-specified derivatives are correct
% optOptions=optimset(optOptions,'LargeScale','off');
optOptions=optimset(optOptions,'TolFun',0.001);
optOptions=optimset(optOptions,'TolCon',0.001);
optOptions=optimset(optOptions,'MaxFunEvals',1000000);
optOptions=optimset(optOptions,'DiffMinChange',1e-6);                % minimum change in variables for finite difference gradients
optOptions=optimset(optOptions,'DiffMaxChange',1e-6);                % maximum change in variables for finite difference gradients

n = evalin('base','n_non');
m = evalin('base','m_non');
Tdelay_assumed = evalin('base','Tdelay_assumed_non');

k_n = find(n_array == n);                               % this step is for keeping the orginial script                
k_m = find(m_array == m);                               % but also allow the user to give his own den,nom,delay
k_delay = find(Tdelay_assumed_array == Tdelay_assumed); 

fitting_RMS_error = fitting_RMS_error_array(k_delay,k_n,k_m);

% graphically show where the fit is, in terms of optimality %
figure(30); clf; zoom on;
contourf(n_array, m_array, log10(squeeze(fitting_RMS_error_array(k_delay,:,:)))'); hold on;
caxis([log_smallest_RMS_error , log_largest_RMS_error]); view(0,90); colorbar;
title(['Log10 of model fitting RMS error (Tdelay = ' num2str(Tdelay_assumed_array(k_delay)) ' [s])' ] );
xlabel('Remaining dyn. denominator order: n [ ]');     ylabel('Remaining dyn. numerator order: m [ ]');
grid on;

plot(n_array(k_n), m_array(k_m) , 'ro'); plot(n_array(k_n), m_array(k_m) , 'yx');
text_handle = text(n_array(k_n), m_array(k_m), [num2str(log10(fitting_RMS_error))]);
set(text_handle,'Color',[1 1 1]);
disp(['log10 of RMS fitting error: ' num2str(log10(fitting_RMS_error))]);

% reconstruct fitting parameters and required graphs %
Tdelay_assumed = Tdelay_assumed_array(k_delay);
n = n_array (k_n); m = m_array (k_m);

disp('delay , den_order , num_order');
[Tdelay_assumed n m]

flag_show_figures = 0;

remove_assumed_delay_from_frf01;                                            % remove delay and show TF
fit_vibration_modes_compute_remainder_frf01;
fit_remaining_dynamics_note_error03;
readjust_coefficients_note_error03;

% sort complex and real poles %
[residue_rem_dyn, pole_rem_dyn, direct_rem_dyn] = residue(b_fit,a_fit)

gamma_k = [];
for k=1: length(pole_rem_dyn)
    if imag(pole_rem_dyn(k))== 0
        X = residue_rem_dyn (k);
        gamma_k = [gamma_k X];
    end
    X=0;
end

% Remainder (i.e. direct terms)
delta_k = direct_rem_dyn;
        
% ========================================= %
% assemble vector of optimization variables %
% ========================================= %

% prepare vector of optimization variables %
Nc = Nmode;                                             % initial number of complex conjugate poles
Nr = 0;                                                 % initial number of real poles
Nd = length(direct_rem_dyn);                            % number of direct terms

optim_var_vector = [];
real_range = [];

% append the already identified complex conjugate poles (from the fitted modes)
for k = 1:Nmode
    optim_var_vector = [optim_var_vector ; wn(k)];
    optim_var_vector = [optim_var_vector ; zeta(k)];
end
wn_range   = (2*((1:Nmode) - 1) + 1)';
zeta_range = (2*((1:Nmode) - 1) + 2)';
p_range = [];

pole_number = 1;
pole_real = []; 
while pole_number <= length(pole_rem_dyn)
    
    s = pole_rem_dyn(pole_number);                                          % read the next pole related to identified remainder dynamics
    if ~isreal(s)                                                           % if we read a complex pole
        wn_tmp = abs(s);                                                    % extract natural frequency information
        z_tmp  = -real(s) / abs(s);                                         % extract damping information        
        optim_var_vector = [optim_var_vector ; wn_tmp];                     % register wn and zeta into optimization variables' array
        optim_var_vector = [optim_var_vector ; z_tmp];        
        wn_range   = [wn_range ; max(wn_range) + 2];
        zeta_range = [zeta_range ; max(zeta_range) + 2];
        Nc = Nc + 1;                                                        % increase the number of complex conjugate pole pairs in the optimization vector
        pole_number = pole_number + 2;                                      % skip checking the next pole, assuming that it is just the conjugate, per the 'residue' command
        
    else                                                                    % if the pole that is read is real
        
        pole_real = [pole_real ; -s];                                       % store real pole in another array, to be appended to the optimization variables at the very end
        Nr = Nr + 1;                                                        % increase the number of real pole to be searched
        pole_number = pole_number + 1;                                      % move on to the next pole from the identified 'remainder' dynamics
    end
    
end

optim_var_vector = [optim_var_vector ; pole_real];                          % append (inverse of) real poles
p_range = [(max(zeta_range)+1):length(optim_var_vector)];                   % index to the real poles inside the optimization variable

optim_var_vector = [optim_var_vector ; Tdelay_assumed];
Tdelay_range = 2*Nc + Nr + 1;

optim_var_range = optim_var_vector;                                         % normalization factors for the optimization vector

% normalized optimization vector %
optim_var_vector_norm = (1./optim_var_range).*optim_var_vector;

% auxiliary variables used in objective function evaluation %
optim_parameters.optim_var_range = optim_var_range;
optim_parameters.wn_range        = wn_range;
optim_parameters.zeta_range      = zeta_range;
optim_parameters.p_range         = p_range;
optim_parameters.Tdelay_range    = Tdelay_range;
optim_parameters.Nc              = Nc;
optim_parameters.Nr              = Nr;
optim_parameters.Nd              = Nd;

[flag_ok] = check_solution_feasibility02(optim_var_vector_norm, optim_parameters);
if flag_ok == 0, error('something is wrong!'); end

w1 = w(data_range);                                                         % selected frequency range
w2 = w1.*w1;                                                                % w1 was already definde (above) as the global fitting frequency array

% these variables may need to be static to the function %
optim_parameters.Nrange          = Nrange;
optim_parameters.data_range      = data_range;
optim_parameters.w1              = w1;
optim_parameters.w2              = w2;

optim_parameters.Phi             = Phi;
optim_parameters.alpha_k         = alpha_k;
optim_parameters.beta_k          = beta_k;
optim_parameters.gamma_k         = gamma_k;
optim_parameters.delta_k         = delta_k;

optim_parameters.Hdelay_removed  = Hdelay_removed;
optim_parameters.Hfull           = Hfull;

% test objective function %
[rms_fit_error] = evaluate_objective_function02(optim_var_vector_norm, optim_parameters)

% % apply optimization to improve the curve fit %
% beep;
% disp('press any key to start nonlinear optimization of model parameters');
% pause;

new_normalized_parameters = fminunc('evaluate_objective_function02',optim_var_vector_norm,optOptions,optim_parameters);
optim_var_vector_norm = new_normalized_parameters;

% new_normalized_parameters = fmincon('evaluate_objective_function01',optim_var_vector_norm,[],[],[],[],zeros(2*Nc + Nr + Nd + 1,1),100*ones(2*Nc + Nr + Nd + 1,1), [],optim_var_vector_norm,optOptions,optim_parameters);
% optim_var_vector_norm = new_normalized_parameters;

[flag_ok] = check_solution_feasibility02(optim_var_vector_norm, optim_parameters);
if flag_ok == 0, error('something is wrong!'); end


%% show the result with the new parameters %
%  ======================================= %
optim_var_vector = optim_var_vector_norm .* optim_var_range;

% extract individual entries of the paramter vector %
wn   = optim_var_vector(wn_range);                                          % the wn and zeta values (from both the modes and 'remainder' dynamics)
zeta = optim_var_vector(zeta_range);
p    = optim_var_vector(p_range);                                           % real pole values
Tdelay_assumed = optim_var_vector(Tdelay_range);                            % assumed delay value

% ======================================================================= %
% construct portion of the regressor matrix related to complex pole pairs %
% ======================================================================= %
for k = 1:Nc

    % FRF contribution of mode k (assuming unit mass) %
    g_k = (wn(k)*wn(k) - w2) ./ ((wn(k).*wn(k) - w2).*(wn(k).*wn(k) - w2) + (2*zeta(k)*wn(k)*w1).*(2*zeta(k)*wn(k)*w1));
    h_k = (-2*zeta(k)*wn(k)*w1) ./ ((wn(k).*wn(k) - w2).*(wn(k).*wn(k) - w2) + (2*zeta(k)*wn(k)*w1).*(2*zeta(k)*wn(k)*w1));
    
    % update portions of regressor matrix related to the real and imaginary
    % components of the dynamic response
    Phi(         1:    Nrange , 2*(k-1) + (1:2)) = [g_k , -w1.*h_k];
    Phi((Nrange+1):(2*Nrange) , 2*(k-1) + (1:2)) = [h_k ,  w1.*g_k];

end

% ======================================================================= %
% construct portion of the regressor matrix related to real poles         %
% ======================================================================= %
for k = 1:Nr

    % FRF contribution of pole k %
    t_k = p(k) ./ ( p(k)*p(k) + w2);
    u_k = -w1  ./ ( p(k)*p(k) + w2);
    
    % update portions of regressor matrix related to the real and imaginary
    % components of the dynamic response
    Phi(         1:    Nrange , 2*Nc + k ) = [t_k];
    Phi((Nrange+1):(2*Nrange) , 2*Nc + k ) = [u_k];

end

% ======================================================================= %
% construct portion of the regressor matrix related to direct terms       %
% ======================================================================= %
w_power = ones(Nrange,1);
for k = 1:Nd

    if mod(k,4) == 1
        Phi(         1:    Nrange , 2*Nc + Nr + k ) = w_power;
        Phi( (Nrange+1):(2*Nrange), 2*Nc + Nr + k ) = zeros(Nrange,1);
    end
    
    if mod(k,4) == 2
        Phi(         1:    Nrange , 2*Nc + Nr + k ) = zeros(Nrange,1);
        Phi( (Nrange+1):(2*Nrange), 2*Nc + Nr + k ) = w_power;
    end
    
    if mod(k,4) == 3
        Phi(         1:    Nrange , 2*Nc + Nr + k ) = -w_power;
        Phi( (Nrange+1):(2*Nrange), 2*Nc + Nr + k ) =  zeros(Nrange,1);
    end
    
    if mod(k,4) == 0
        Phi(         1:    Nrange , 2*Nc + Nr + k ) = zeros(Nrange,1);
        Phi( (Nrange+1):(2*Nrange), 2*Nc + Nr + k ) = -w_power;
    end
    
    w_power = w_power .* w1;

end

% output vector %
Y = [real(Hdelay_removed(data_range)) ; imag(Hdelay_removed(data_range)) ]; % real and imaginary components of 'measured' FRF after assumed delay removal

% Basic LS - no weighting %
theta = pinv(Phi)*Y;

% weighted LS %
% W = eye(2*Nrange);                                                          % do not use any weights
% W = diag([1./abs(Hdelay_removed(data_range)) ; 1./abs(Hdelay_removed(data_range))]);                                  % use reciprocal of measured TF amplitude as weighting function
% W = diag([1./(abs(Hdelay_removed(data_range)).*abs(Hdelay_removed(data_range))) ; 1./(Hdelay_removed(Hrem(data_range)).*abs(Hdelay_removed(data_range)))]);        % use square of reciprocal of measured TF amplitude as weighting function
% theta = inv(Phi'*W*Phi)*Phi'*W*Y;                                           % solve parameters

% extract identified parameters into new TF array %
for k = 1:Nc
    alpha_k(k) = theta(2*(k-1) + 1);
    beta_k(k) =  theta(2*(k-1) + 2);
end

for k = 1:Nr
    gamma_k(k) = theta(2*Nc + k);
end

for k = 1:Nd
    delta_k(k) = theta(2*Nc + Nr + k);
end

% estimate predicted response %
Yest = Phi*theta;

% delay effect %
Hdelay_assumed_data_range = cos(w1*Tdelay_assumed) - j*sin(w1*Tdelay_assumed);
Yest_delay = Hdelay_assumed_data_range .* (Yest(1:Nrange) + j*Yest((Nrange+1):(2*Nrange)));

% estimate RMS error %
E = Hfull(data_range) - Yest_delay;
% E = Phi*theta - Y;
J = sqrt(E'*E/(Nrange));

% ======================================================================= %
% reconstruct new model                                                   %
% ======================================================================= %


% ================================== %
% construct the FRF of the new model %
% ================================== %
G_fit = ss(tf(0,1));
save_and_assign_nonlinear;
if show_final_figures
    
    % contribution of 'newly tuned' modes %
    component_FRF = zeros(length(w),Nc + Nr + Nd);
    total_modeled_TF = zeros(length(w),1);
    
    % complex poles %
    for k = 1:Nc
        
        % FRF contribution %
        g_k = (wn(k)*wn(k) - w.*w) ./ ((wn(k).*wn(k) - w.*w).*(wn(k).*wn(k) - w.*w) + (2*zeta(k)*wn(k)*w).*(2*zeta(k)*wn(k)*w));
        h_k = (-2*zeta(k)*wn(k)*w) ./ ((wn(k).*wn(k) - w.*w).*(wn(k).*wn(k) - w.*w) + (2*zeta(k)*wn(k)*w).*(2*zeta(k)*wn(k)*w));
        component_FRF(:,k) = (alpha_k(k) + j*w*beta_k(k)).*(g_k + j*h_k);
        
        % TF contribution %
        G_fit = G_fit + ss(tf([beta_k(k) , alpha_k(k)],[1 2*zeta(k)*wn(k) wn(k)*wn(k)]));
        
        % add the contribution of the current mode to the 'modeled' FRF %
        total_modeled_TF = total_modeled_TF + component_FRF(:,k);
    end

    % real poles %
    for k = 1:Nr
        
        % FRF contribution of pole k %
        t_k = p(k) ./ ( p(k)*p(k) + w.*w);
        u_k = -w   ./ ( p(k)*p(k) + w.*w);
        component_FRF(:,Nc+k) = (gamma_k(k)).*(t_k + j*u_k);
        
        % TF contribution %
        G_fit = G_fit + ss(tf([gamma_k(k)],[1 p(k)]));

        % add the contribution of the current mode to the 'modeled' FRF %
        total_modeled_TF = total_modeled_TF + component_FRF(:,Nc+k);
    end
    
    % direct terms %
    jw_power = ones(N,1);
    D = tf(1,1);
    for k = 1:Nd
        
        % FRF contribution of direct term of power k-1 %
        component_FRF(:,Nc+Nr+k) = delta_k(k).*jw_power;
        
        % TF contribution %
        G_fit = G_fit + ss(delta_k(k) * D);
        D = D * tf([1 0],[1]);

        % add the contribution of the current mode to the 'modeled' FRF %
        total_modeled_TF = total_modeled_TF + component_FRF(:,Nc+Nr+k);
        jw_power = jw_power .* (j*w);
    end
    
    % combine fitted remainder TF with fitted contributions from the modes %
    total_modeled_TF_w_delay = total_modeled_TF.*Hdelay_assumed;
    G_fit_w_delay = G_fit;
    G_fit_w_delay.InputDelay = Tdelay_assumed;
    total_modeled_TF_w_delay2 = squeeze(freqresp(G_fit_w_delay,w));

    % compute fitting error, and its RMS value in selected frequency range %]
    fitting_error = Hfull - total_modeled_TF_w_delay;
    fitting_error_RMS_selected_freq_range = sqrt( fitting_error(data_range)' * fitting_error(data_range) / Nrange );
    
    
    % show the individual contributions %
%     for k = 1:(Nc+Nr+Nd)
%         
%         figure(5);
%         subplot(2,1,1); loglog(f,abs(component_FRF(:,k)),'g');
%         ylabel('mag [ ]'); title('Bode Plot (log freq and mag axes)');
%         tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
%         subplot(2,1,2); semilogx(f,r2d*unwrap(angle(component_FRF(:,k))),'g');
%         ylabel('pha [deg]'); xlabel('freq [Hz]');
%         tmp = axis; axis([fview_min fview_max -200 200]);
%         
%         figure(6);
%         subplot(2,1,1); plot(f,abs(component_FRF(:,k)),'g');
%         ylabel('mag [ ]'); title('Bode Plot (linear freq and mag axes)');
%         tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
%         subplot(2,1,2); plot(f,r2d*unwrap(angle(component_FRF(:,k))),'g');
%         ylabel('pha [deg]'); xlabel('freq [Hz]');
%         tmp = axis; axis([fview_min fview_max -200 200]);
%         
%         figure(7);
%         subplot(2,1,1); semilogx(f, real(component_FRF(:,k)), 'g');
%         ylabel('real [ ]'); title('Real & Imaginary Components');
%         tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%         subplot(2,1,2); semilogx(f, imag(component_FRF(:,k)), 'g');
%         ylabel('imag [ ]'); xlabel('freq [Hz]');
%         tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%         
%         figure(8);
%         plot(real(component_FRF(:,kmode)), imag(component_FRF(:,k)), 'g');
%         xlabel('real [ ]'); ylabel('imag [ ]'); title('Real & Imaginary Components (selected viewing data range)');
%         axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
%         
% %         pause;
%         
%     end

    figure(9); clf; zoom on;
    subplot(2,1,1); loglog(f,abs(Hdelay_removed),'k'); hold on;
    subplot(2,1,1); loglog(f,abs(total_modeled_TF),'b');
    legend( 'measurement with assumed delay removed','fitted model');
    ylabel('mag [ ]'); grid on;
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hdelay_removed),'k')); hold on;
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(total_modeled_TF)),'b');
    ylabel('pha [deg]'); xlabel('freq [Hz]'); grid on;
    tmp = axis; axis([fview_min fview_max -200 200]);
    
    figure(10); clf; zoom on;
    subplot(2,1,1); plot(f,abs(Hdelay_removed),'k',f,abs(total_modeled_TF),'b.');
    legend( 'measurement with assumed delay removed','fitted model');
    ylabel('mag [ ]'); grid on;
    tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
    subplot(2,1,2); plot(f,r2d*unwrap(angle(Hdelay_removed)),'k', f,r2d*unwrap(angle(total_modeled_TF)),'b.');
    tmp = axis; axis([fview_min fview_max -200 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]'); grid on;
    
    figure(11); clf; zoom on;
    subplot(2,1,1); semilogx(f,real(Hdelay_removed),'k'); hold on;
    subplot(2,1,1); semilogx(f, real(total_modeled_TF), 'b');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    title('Test Data and Fitted TF'); ylabel('real [ ]'); grid on;
    legend( 'measurement with assumed delay removed','fitted model');
    
    subplot(2,1,2); semilogx(f,imag(Hdelay_removed),'k'); hold on;
    subplot(2,1,2); semilogx(f, imag(total_modeled_TF), 'b');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]'); grid on;
    
    figure(12); clf; zoom on;
    plot(real(Hdelay_removed(data_range)), imag(Hdelay_removed(data_range)), 'k', real(total_modeled_TF(data_range)), imag(total_modeled_TF(data_range)), 'b.');
    legend('measurement with assumed delay removed','fitted model'); xlabel('Re{G}'); ylabel('Im{G}');
    
    % compare overall fitted TF with originally 'measured' data %
    figure(1);
    subplot(2,1,1);
    loglog(f, abs(Hfull),'k',  f, abs(total_modeled_TF_w_delay), 'c',  f, abs(total_modeled_TF_w_delay2), 'm--');
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    ylabel('mag [ ]'); title('Bode Plot (log freq and mag axes)'); grid on;
    subplot(2,1,2);
    semilogx(f, r2d*unwrap(angle(Hfull)),'k', f, r2d*unwrap(angle(total_modeled_TF_w_delay)), 'c',  f, r2d*unwrap(angle(total_modeled_TF_w_delay2)), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
    tmp = axis; axis([fview_min fview_max -200 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]'); grid on;
    
    figure(2);
    subplot(2,1,1);
    plot(f, abs(Hfull),'k', f, abs(total_modeled_TF_w_delay), 'c' , f, abs(total_modeled_TF_w_delay2), 'm--');
    tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
    ylabel('mag [ ]'); title('Bode Plot (linear freq and mag axes)'); grid on;
    subplot(2,1,2);
    plot(f, r2d*unwrap(angle(Hfull)), 'k', f, r2d*unwrap(angle(total_modeled_TF_w_delay)), 'c', f, r2d*unwrap(angle(total_modeled_TF_w_delay2)), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','analytical model','Location','Best');
    tmp = axis; axis([fview_min fview_max -200 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]'); grid on;
    
    figure(3);
    subplot(2,1,1);
    semilogx(f, real(Hfull), 'k', f, real(total_modeled_TF_w_delay), 'c', f, real(total_modeled_TF_w_delay2), 'm--');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [ ]'); title('Real & Imaginary Components'); grid on;
    subplot(2,1,2);
    semilogx(f, imag(Hfull), 'k', f, imag(total_modeled_TF_w_delay), 'c', f, imag(total_modeled_TF_w_delay2), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','analytical model','Location','Best');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]'); grid on;

    figure(13); clf; zoom on;
    subplot(2,1,1);
    semilogx(f, real(fitting_error),'k', f(data_range),real(fitting_error(data_range)),'b');
    title('Fitting Error'); ylabel('Real [ ]'); legend('Complete Data','Selected Range');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]); grid on;
    subplot(2,1,2);
    semilogx(f, imag(fitting_error),'k', f(data_range),imag(fitting_error(data_range)),'b');
    ylabel('Imag [ ]'); xlabel('freq [Hz]');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]); grid on;    
    
    figure(14); clf; zoom on;
    pzmap(G_fit_w_delay); title('P-Z Map of the Identified Dynamics'); 
    

end