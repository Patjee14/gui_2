%% fit noticeable modes  

% allocate arrays %
kmin = zeros(Nmode,1);    kmax = zeros(Nmode,1);                            % data selection index
wn = zeros(Nmode,1);      zeta = zeros(Nmode,1);                            % wn and zeta
a = zeros(Nm,Nn,Nmode);   b =  zeros(Nm,Nn,Nmode);                         % residual terms
c = zeros(Nm,Nn,Nmode);   d = zeros(Nm,Nn,Nmode);         
alpha_k = zeros(Nm,Nn,Nmode);  beta_k  = zeros(Nm,Nn,Nmode);                          % residues (i.e. modal constants, mode participation factors)
 
for kmode = 1:Nmode
    
    % record indices and display selection data %
    % ========================================= %
    kmin(kmode) = ceil(interp1(f,(1:N)',fmin(kmode)));
    kmax(kmode) = floor(interp1(f,(1:N)',fmax(kmode)));
    
    data_range = (kmin(kmode):kmax(kmode))';
    Nrange = length(data_range);                                            % number of samples in selected range
    
    g = zeros(Nm,Nn,Nrange);
    h = zeros(Nm,Nn,Nrange);
    
    if flag_show_figures
    
        k = 1;
        
        for  i = 1:Nm
            for j = 1:Nn 
        figure(5); 
        subplot(2*Nm,Nn,k); loglog(f(data_range),abs(squeeze(Hdelay_removed(i,j,data_range))),'b'); 
        title(['G' num2str(i) num2str(j)]);
%         tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
        subplot(2*Nm,Nn,k+Nn); semilogx(f(data_range),r2d*unwrap(angle(squeeze(Hdelay_removed(i,j,data_range)))),'b');
%         tmp = axis; axis([fview_min fview_max -1000 200]);
            
        figure(6); 
        subplot(2*Nm,Nn,k); plot(f(data_range),abs(squeeze(Hdelay_removed(i,j,data_range))),'b');
        title(['G' num2str(i) num2str(j)]);
%         tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
        subplot(2*Nm,Nn,k+Nn); plot(f(data_range),r2d*unwrap(angle(squeeze(Hdelay_removed(i,j,data_range)))),'b');
%         tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(7); 
        subplot(2*Nm,Nn,k); semilogx(f(data_range),real(squeeze(Hdelay_removed(i,j,data_range))),'b');
        title(['G' num2str(i) num2str(j)]);
%         tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        subplot(2*Nm,Nn,k+Nn); semilogx(f(data_range),imag(squeeze(Hdelay_removed(i,j,data_range))),'b');
%         tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        k = k + 1;
        end
        k = 2*Nn+1+(i-1)*Nn*2; 
    end
        
        figure(8); clf; 
        k = 1;
        for  i = 1:Nm
            for j = 1:Nn 
        subplot(Nm,Nn,k);
        plot(real(Hdelay_removed(data_range)), imag(squeeze(Hdelay_removed(i,j,data_range))), 'b');
        title(['G' num2str(i) num2str(j)]);
%         axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
        k = k + 1; 
            end
        end
    end
    
    % fit modal parameters %
    % ==================== %
    
    % prepare arrays for convenience %
    
    f1 = f(data_range);
    w1 = w(data_range); w2 = w1.*w1; w3 = w1.*w2;                           % positive and negative powers of frequency
    invw1 = 1./w1;     invw2 = 1./w2;
    One = ones(Nrange,1); Zero = zeros(Nrange,1);                     % arrays of ones and zeros (for constructing the regressor matrix)
    Y = [];
    Phi1 = [];
    Phi2 = zeros(2*Nm*Nn*Nrange,2*Nm*Nn);
    Phi3 = [];
    Phi4 = zeros(2*Nm*Nn*Nrange,2*Nm*Nn);   % make according size for regresson matrix
    Phi5 = zeros(2*Nm*Nn*Nrange,2*Nm*Nn);
    Phi6 = zeros(2*Nm*Nn*Nrange,2*Nm*Nn);
    
    for k = 1:Nrange
        for q = 1:Nm*Nn                     %to fill this part of the regresson matrix
    Phi2(k+2*Nrange*(q-1),q) = -invw2(k); 
    Phi2(k+Nrange+2*Nrange*(q-1),q+Nm*Nn) = -invw2(k);
    Phi4(k+Nrange+2*Nrange*(q-1),q) = -invw1(k);
    Phi4(k+2*Nrange*(q-1),q+Nm*Nn) = invw1(k);
    Phi5(k+2*Nrange*(q-1),q) = One(k); 
    Phi5(k+Nrange+2*Nrange*(q-1),q+Nm*Nn) = One(k);
    Phi6(k+2*Nrange*(q-1),q) = -invw2(k);
    Phi6(k+Nrange+2*Nrange*(q-1),q+Nm*Nn) = -invw1(k);
        end
    end
    
    for  i = 1:Nm
        for j = 1:Nn 
    g(i,j,:) = real(Hdelay_removed(i,j,data_range));	
    h(i,j,:) = imag(Hdelay_removed(i,j,data_range));                    % real and imaginary components of 'measured' FRF
     
    % assembling the Phi components
    Phi1 = [Phi1; [invw2.*squeeze(g(i,j,:)); invw2.*squeeze(h(i,j,:))]];
    Phi3 = [Phi3; [-invw1.*squeeze(h(i,j,:)); invw1.*squeeze(g(i,j,:))]];
    Y = [Y; [squeeze(g(i,j,:)) ; squeeze(h(i,j,:))]];
        end
    end
    
%     % initial fit; only used to estimate wn and zeta values %
%     Phi = [invw2.*squeeze(g), -invw2, Zero, -invw1.*squeeze(h), Zero, invw1, One, Zero, -invw2, Zero; ...
%     invw2.*squeeze(h), Zero, -invw2, invw1.*squeeze(g), -invw1, Zero, Zero, One, Zero, -invw1];
    Phi = [Phi1,Phi2,Phi3,Phi4,Phi5,Phi6];
    theta = pinv(Phi)*Y;
          
    wn(kmode) = real(sqrt(theta(1)));
    zeta(kmode) = theta(2+2*Nm*Nn)/(2*wn(kmode));
    
    % after having approximately identified wn and zeta          % 
    % re-fit residual terms and residues (i.e., real and complex %
    % participation factors for the mode being identified        %
    % 2nd order response for term representing the mode
    % G2 = 1 / (s2 + 2*z*wn*s + wn^2) %
    g2 = (wn(kmode)*wn(kmode) - w2) ./ ((wn(kmode).*wn(kmode) - w2).*(wn(kmode).*wn(kmode) - w2) + (2*zeta(kmode)*wn(kmode)*w1).*(2*zeta(kmode)*wn(kmode)*w1));
    h2 = (-2*zeta(kmode)*wn(kmode)*w1) ./ ((wn(kmode).*wn(kmode) - w2).*(wn(kmode).*wn(kmode) - w2) + (2*zeta(kmode)*wn(kmode)*w1).*(2*zeta(kmode)*wn(kmode)*w1));
    
    residual_f = [];
    mode_f = [];
    total_f = [];
    
    for  i = 1:Nm
        for j = 1:Nn 
            
    % this matrix is constant for every TF        
    Phi = [ -invw2,  Zero, One, Zero, g2, -w1.*h2 ; ...
              Zero, -invw1, Zero, w1, h2, w1.*g2];
    Y = [squeeze(g(i,j,:)) ; squeeze(h(i,j,:))];
    theta = pinv(Phi)*Y;
    
    % extract identified parameters %
    a(i,j,kmode)       = theta(1); b(i,j,kmode)      = theta(2); c(i,j,kmode) = theta(3); d(i,j,kmode) = theta(4);                 % residual terms
    alpha_k(i,j,kmode) = theta(5); beta_k(i,j,kmode) = theta(6);                                  % mode participation factors (residues)

    % contruct residual component, the mode, and their combination %
    residual_f = [residual_f,[-invw2*a(i,j,kmode) + (-1i*invw1)*b(i,j,kmode) + c(i,j,kmode)*One + (1i*w1)*d(i,j,kmode)]];
    mode_f  = [mode_f,[(alpha_k(i,j,kmode)*One + 1i*w1*beta_k(i,j,kmode)).*(g2+1i*h2)]];
    total_f = [total_f [(residual_f + mode_f)]];
    
        end
    end

    if flag_show_figures
        
       k = 1;
       q = 1;
       for  i = 1:Nm
           for j = 1:Nn
    
        % show captured response %
        figure(5); 
        subplot(2*Nm,Nn,k); loglog(f1,abs(residual_f(:,q)),'c');
        title(['G' num2str(i) num2str(j)]);
        subplot(2*Nm,Nn,k+Nn); semilogx(f1,r2d*unwrap(angle(residual_f(:,q))),'c');
        subplot(2*Nm,Nn,k); loglog(f1,abs(mode_f(:,q)),'g');
        subplot(2*Nm,Nn,k+Nn); semilogx(f1,r2d*unwrap(angle(mode_f(:,q))),'g');
        subplot(2*Nm,Nn,k); loglog(f1,abs(total_f(:,q)),'r');
%         tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
        subplot(2*Nm,Nn,k+Nn); semilogx(f1,r2d*unwrap(angle(total_f(:,q))),'r');
%         tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(6); 
        subplot(2*Nm,Nn,k); plot(f1,abs(residual_f(:,q)),'c'); 
        title(['G' num2str(i) num2str(j)]);
        subplot(2*Nm,Nn,k+Nn); plot(f1,r2d*unwrap(angle(residual_f(:,q))),'c');
        subplot(2*Nm,Nn,k); plot(f1,abs(mode_f(:,q)),'g');
        subplot(2*Nm,Nn,k+Nn); plot(f1,r2d*unwrap(angle(mode_f(:,q))),'g');
        subplot(2*Nm,Nn,k); plot(f1,abs(total_f(:,q)),'r');
%         tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
        subplot(2*Nm,Nn,k+Nn); plot(f1,r2d*unwrap(angle(total_f(:,q))),'r');
%         tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(7); 
        subplot(2*Nm,Nn,k); semilogx(f1, real(residual_f(:,q)), 'c'); 
        title(['G' num2str(i) num2str(j)]);
        subplot(2*Nm,Nn,k+Nn); semilogx(f1, imag(residual_f(:,q)), 'c');
        subplot(2*Nm,Nn,k); semilogx(f1, real(mode_f(:,q)), 'g');
        subplot(2*Nm,Nn,k+Nn); semilogx(f1, imag(mode_f(:,q)), 'g');
        subplot(2*Nm,Nn,k); semilogx(f1, real(total_f(:,q)), 'r');
%         tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        subplot(2*Nm,Nn,k+Nn); semilogx(f1, imag(total_f(:,q)), 'r');
%         tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        k = k + 1;
        q = q + 1;
            end
        k = 2*Nn+1+(i-1)*Nn*2; 
        end
        
        figure(8); clf;
        q = 1;
        for  i = 1:Nm
           for j = 1:Nn
        subplot(Nm,Nn,q); 
        plot(real(residual_f(:,q)), imag(residual_f(:,q)), 'c', real(mode_f(:,q)), imag(mode_f(:,q)), 'g', real(total_f(:,q)), imag(total_f(:,q)), 'r');
        q = q + 1;
           end
        end
    end
    
    % pause;
    
end

% reconstruct the contribution of the identified modes %
mode_FRF = [];
total_modeled_modes_FRF = zeros(Nm,Nn,length(w));
q = 1;

for kmode = 1:Nmode
    k = 1;        
    g2 = (wn(kmode)*wn(kmode) - w.*w) ./ ((wn(kmode).*wn(kmode) - w.*w).*(wn(kmode).*wn(kmode) - w.*w) + (2*zeta(kmode)*wn(kmode)*w).*(2*zeta(kmode)*wn(kmode)*w));
    h2 = (-2*zeta(kmode)*wn(kmode)*w) ./ ((wn(kmode).*wn(kmode) - w.*w).*(wn(kmode).*wn(kmode) - w.*w) + (2*zeta(kmode)*wn(kmode)*w).*(2*zeta(kmode)*wn(kmode)*w));
    
    for  i = 1:Nm
        for j = 1:Nn
    mode_FRF = [mode_FRF, [(alpha_k(i,j,kmode) + 1i*w*beta_k(i,j,kmode)).*(g2 + 1i*h2)]];
    
    % add the contribution of the current mode to the 'modeled' FRF %
    total_modeled_modes_FRF(i,j,:) = squeeze(total_modeled_modes_FRF(i,j,:)) + mode_FRF(:,q);
   
    if flag_show_figures 
             
        figure(5);
        subplot(2*Nm,Nn,k); loglog(f,abs(mode_FRF(:,q)),'g:');
        title(['G' num2str(i) num2str(j)]);
%         tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
        subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(mode_FRF(:,q))),'g:');
%         tmp = axis; axis([fview_min fview_max  -1000 200]);
        
        figure(6);
        subplot(2*Nm,Nn,k); plot(f,abs(mode_FRF(:,q)),'g:');
        title(['G' num2str(i) num2str(j)]);
%         tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
        subplot(2*Nm,Nn,k+Nn); plot(f,r2d*unwrap(angle(mode_FRF(:,q))),'g:');
%         tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(7);
        subplot(2*Nm,Nn,k); semilogx(f, real(mode_FRF(:,q)), 'g:');
        title(['G' num2str(i) num2str(j)]);
%         tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
        subplot(2*Nm,Nn,k+Nn); semilogx(f, imag(mode_FRF(:,q)), 'g:');
%         tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
                               
%         figure(8); 
%         subplot(Nn,Nm,q);
%         plot(real(mode_FRF(:,q)), imag(mode_FRF(:,q)), 'g:');
%         axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
    end
        q = q + 1;
        k = k + 1;
        end 
        k = 2*Nn+1+(i-1)*Nn*2;
    end

    % pause;
end

% if flag_show_figures,
%     
%     figure(9); clf; zoom on;
%     subplot(2,1,1); loglog(f,abs(Hdelay_removed),'k',w,abs(total_modeled_modes_FRF),'b.');
%     tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
%     subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hdelay_removed)),'k', w,r2d*unwrap(angle(total_modeled_modes_FRF)),'b.');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
%     
%     figure(10); clf; zoom on;
%     subplot(2,1,1); plot(f,abs(Hdelay_removed),'k',w,abs(total_modeled_modes_FRF),'b.');
%     tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
%     subplot(2,1,2); plot(f,r2d*unwrap(angle(Hdelay_removed)),'k', w,r2d*unwrap(angle(total_modeled_modes_FRF)),'b.');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
%     
%     figure(11); clf; zoom on;
%     subplot(2,1,1); semilogx(f, real(Hdelay_removed), 'k', w, real(total_modeled_modes_FRF), 'b.');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
%     subplot(2,1,2); semilogx(f, imag(Hdelay_removed), 'k', w, imag(total_modeled_modes_FRF), 'b.');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
%     
%     figure(12); clf; zoom on;
%     plot(real(Hdelay_removed), imag(Hdelay_removed), 'k', real(total_modeled_modes_FRF), imag(total_modeled_modes_FRF), 'b.');
%     axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
% 
% end

% investigate the discrepancy (remainder) terms between the 'measured' FRF 
% and the fitted modes 

k = 1;
for  i = 1:Nm
    for j = 1:Nn 
        
Hrem(i,j,:) = Hdelay_removed(i,j,:) - total_modeled_modes_FRF(i,j,:);

if flag_show_figures

    figure(9); zoom on;
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(total_modeled_modes_FRF(i,j,:))),'b');
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hrem(i,j,:))),'r');
    legend('measurement with assumed delay removed','fitted modes','remainder term');
%     tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    title(['G' num2str(i) num2str(j)]);
    ylabel('mag [m/N]');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hdelay_removed(i,j,:)))),'k'); hold on;
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(total_modeled_modes_FRF(i,j,:)))),'b');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hrem(i,j,:)))),'r');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(11); zoom on;
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    ylabel('real [m/N]');
    subplot(2*Nm,Nn,k); semilogx(f, real(squeeze(total_modeled_modes_FRF(i,j,:))), 'b');
    subplot(2*Nm,Nn,k); semilogx(f, real(squeeze(Hrem(i,j,:))), 'r');
    legend('measurement with assumed delay removed','fitted modes','remainder term');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    title(['G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    ylabel('imag [m/N]'); xlabel('freq [Hz]');
    subplot(2*Nm,Nn,k+Nn); semilogx(f, imag(squeeze(total_modeled_modes_FRF(i,j,:))), 'b');
    subplot(2*Nm,Nn,k+Nn); semilogx(f, imag(squeeze(Hrem(i,j,:))), 'r');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])

end
k = k + 1;
    end
k = 2*Nn+1+(i-1)*Nn*2; 
end
