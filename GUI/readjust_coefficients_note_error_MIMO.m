%% apply global improvement for the mode residuals (i.e., mode participation factors)
%  and numerator coefficients of the fitted remainder dynamics (i.e.,residual)
%  transfer function

% ======================================================================= %
% construct portion of the regressor matrix related to vibration modes%   %
% ======================================================================= %
w2 = w1.*w1;                                                                % w1 was already definde (above) as the global fitting frequency array
Phi = [];

for kmode = 1:Nmode
    
    % FRF contribution of mode k (assuming unit mass) %
    g_k = (wn(kmode)*wn(kmode) - w2) ./ ((wn(kmode).*wn(kmode) - w2).*(wn(kmode).*wn(kmode) - w2) + (2*zeta(kmode)*wn(kmode)*w1).*(2*zeta(kmode)*wn(kmode)*w1));
    h_k = (-2*zeta(kmode)*wn(kmode)*w1) ./ ((wn(kmode).*wn(kmode) - w2).*(wn(kmode).*wn(kmode) - w2) + (2*zeta(kmode)*wn(kmode)*w1).*(2*zeta(kmode)*wn(kmode)*w1));
    
    % update portions of regressor matrix related to the real and imaginary
    % components of the dynamic response
    Phi_real = [g_k , -w1.*h_k];
    Phi_imag = [h_k ,  w1.*g_k];
    Phi = [Phi , [ Phi_real ; Phi_imag]];

end

% ======================================================================= %
% construct portion of the regressor matrix related to the remainder
% dynamics
% ======================================================================= %
% FRF contribution of identified denominator (p & q)%
t = zeros(Nrange,1); u = zeros(Nrange,1);                                   % intermediate arrays to construct FRF of denominator portion
if n>0
    t = t + a_recip_fit(1);
end
% else
%     t = t + 1;


w_power = ones(Nrange,1);

for k = 1:(n-1)
    
    w_power = w_power .* w1;
    
    if mod(k,4) == 1
        u = u + w_power * a_recip_fit(k+1);
    end
    
    if mod(k,4) == 2
        t = t - w_power * a_recip_fit(k+1);
    end
    
    if mod(k,4) == 3
        u = u - w_power * a_recip_fit(k+1);
    end
    
    if mod(k,4) == 0
        t = t + w_power * a_recip_fit(k+1);
    end
end

if n>0
    k = n;
    w_power = w_power .* w1;
    if mod(k,4) == 1, u = u + w_power ; end
    if mod(k,4) == 2, t = t - w_power ; end
    if mod(k,4) == 3, u = u - w_power ; end
    if mod(k,4) == 0, t = t + w_power ; end
end


if n>0
p =  t ./ (t.*t + u.*u);
q = -u ./ (t.*t + u.*u);
else
    p = ones(Nrange,1);
    q = zeros(Nrange,1);
end

% % just verification %
% Rfit = tf([1],[a_fit]);
% Rf = squeeze(freqresp(Rfit,w1));
% figure(20); clf; zoom on;
% subplot(2,1,1); plot(w1,p,'b', w1,real(Rf),'r--');
% subplot(2,1,2); plot(w1,q,'b', w1,imag(Rf),'r--');
% 
% pause;

Phi_real = [p];
Phi_imag = [q];
Phi = [Phi , [Phi_real ; Phi_imag] ];

w_power = ones(Nrange,1);
for k = 1:m
    
    w_power = w_power .* w1;

    if mod(k,4) == 1
        Phi_real = -w_power .* q;
        Phi_imag =  w_power .* p;
        Phi = [Phi , [Phi_real ; Phi_imag] ];
    end
    
    if mod(k,4) == 2
        Phi_real = -w_power .* p;
        Phi_imag = -w_power .* q;
        Phi = [Phi , [Phi_real ; Phi_imag] ];
    end
    
    if mod(k,4) == 3
        Phi_real =  w_power .* q;
        Phi_imag = -w_power .* p;
        Phi = [Phi , [Phi_real ; Phi_imag] ];
    end
    
    if mod(k,4) == 0
        Phi_real =  w_power .* p;
        Phi_imag =  w_power .* q;
        Phi = [Phi , [Phi_real ; Phi_imag] ];
    end
    
end

for kmode = 1:Nmode
    for  i = 1:Nm
        for j = 1:Nn
% output vector %
Y = [real(squeeze(Hdelay_removed(i,j,data_range))) ; imag(squeeze(Hdelay_removed(i,j,data_range))) ]; % real and imaginary components of 'measured' FRF after assumed delay removal

% Basic LS - no weighting %
theta = pinv(Phi)*Y;

% weighted LS %
% W = eye(2*Nrange);                                                          % do not use any weights
% W = diag([1./abs(Hdelay_removed(data_range)) ; 1./abs(Hdelay_removed(data_range))]);                                  % use reciprocal of measured TF amplitude as weighting function
% W = diag([1./(abs(Hdelay_removed(data_range)).*abs(Hdelay_removed(data_range))) ; 1./(Hdelay_removed(Hrem(data_range)).*abs(Hdelay_removed(data_range)))]);        % use square of reciprocal of measured TF amplitude as weighting function
% theta = inv(Phi'*W*Phi)*Phi'*W*Y;                                           % solve parameters

% extract identified parameters into new TF array %
alpha_k(i,j,kmode) = theta(2*(kmode-1) + 1);
beta_k(i,j,kmode) =  theta(2*(kmode-1) + 2);

for k = 1:(m+1)
    b_recip_fit(k) = theta(2*Nmode + k);
end

b_fit(i,j,:) = (flipud(b_recip_fit'));                                             % numerator as b0*s^m + b1*s^{m-1} + ... + bm
Gremfit(i,j,:) = tf([squeeze(b_fit(i,j,:))'],[a_fit]);
        end 
    end
end
% ======================================================================= %
% reconstruct new model                                                   %
% ======================================================================= %

% contribution of 'newly tuned' modes %
mode_FRF = [];
total_modeled_modes_FRF = zeros(Nm,Nn,length(w));
q = 1;

for kmode = 1:Nmode
    k = 1;
    g2 = (wn(kmode)*wn(kmode) - w.*w) ./ ((wn(kmode).*wn(kmode) - w.*w).*(wn(kmode).*wn(kmode) - w.*w) + (2*zeta(kmode)*wn(kmode)*w).*(2*zeta(kmode)*wn(kmode)*w));
    h2 = (-2*zeta(kmode)*wn(kmode)*w) ./ ((wn(kmode).*wn(kmode) - w.*w).*(wn(kmode).*wn(kmode) - w.*w) + (2*zeta(kmode)*wn(kmode)*w).*(2*zeta(kmode)*wn(kmode)*w));
       
    for  i = 1:Nm
        for j = 1:Nn 
    mode_FRF = [mode_FRF, [(alpha_k(i,j,kmode) + 1i*w*beta_k(i,j,kmode)).*(g2 + 1i*h2)]];
    
    % add the contribution of the current mode to the 'modeled' FRF %
    total_modeled_modes_FRF(i,j,:) = squeeze(total_modeled_modes_FRF(i,j,:)) + mode_FRF(:,q);

    if flag_show_figures
        
        figure(5);
        subplot(2*Nm,Nn,k); loglog(f,abs(mode_FRF(:,q)),'g:');
        ylabel('mag [ ]'); title(['Bode Plot (log freq and mag axes) G' num2str(i) num2str(j)]);
%         tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
        subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(mode_FRF(:,q))),'g:');
        ylabel('pha [deg]'); xlabel('freq [Hz]');
%         tmp = axis; axis([fview_min fview_max -1000 200]);
        set(gcf,'units','normalized','outerposition',[0 0 1 1])
        
        figure(6);
        subplot(2*Nm,Nn,k); plot(f,abs(mode_FRF(:,q)),'g:');
        ylabel('mag [ ]'); title(['Bode Plot (linear freq and mag axes) G' num2str(i) num2str(j)]);
%         tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
        subplot(2*Nm,Nn,k+Nn); plot(f,r2d*unwrap(angle(mode_FRF(:,q))),'g:');
        ylabel('pha [deg]'); xlabel('freq [Hz]');
%         tmp = axis; axis([fview_min fview_max -1000 200]);
        set(gcf,'units','normalized','outerposition',[0 0 1 1])
        
        figure(7);
        subplot(2*Nm,Nn,k); semilogx(f, real(mode_FRF(:,q)), 'g:');
        ylabel('real [ ]'); title(['Real & Imaginary Components G' num2str(i) num2str(j)]);
%         tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
        subplot(2*Nm,Nn,k+Nn); semilogx(f, imag(mode_FRF(:,q)), 'g:');
        ylabel('imag [ ]'); xlabel('freq [Hz]');
%         tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
        set(gcf,'units','normalized','outerposition',[0 0 1 1])
        
%         figure(8);
%         subplot(Nn,Nm,q);
%         plot(real(mode_FRF(:,q)), imag(mode_FRF(:,q)), 'g:');
%         xlabel('real [ ]'); ylabel('imag [ ]'); title(['Real & Imaginary Components (selected viewing data range) G' num2str(i) num2str(j)]);
%         axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
%         set(gcf,'units','normalized','outerposition',[0 0 1 1])

    end
        q = q + 1;
        k = k + 1;
        end 
        k = 2*Nn+1+(i-1)*Nn*2;
    end

    % pause;
end


% if flag_show_figures,
%     
%     figure(9); clf; zoom on;
%     subplot(2,1,1); loglog(w,abs(Hdelay_removed),'k',w,abs(total_modeled_modes_FRF),'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     subplot(2,1,2); semilogx(w,r2d*unwrap(angle(Hdelay_removed)),'k', w,r2d*unwrap(angle(total_modeled_modes_FRF)),'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     
%     figure(10); clf; zoom on;
%     subplot(2,1,1); plot(w,abs(Hdelay_removed),'k',w,abs(total_modeled_modes_FRF),'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     subplot(2,1,2); plot(w,r2d*unwrap(angle(Hdelay_removed)),'k', w,r2d*unwrap(angle(total_modeled_modes_FRF)),'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     
%     figure(11); clf; zoom on;
%     subplot(2,1,1); semilogx(w, real(Hdelay_removed), 'k', w, real(total_modeled_modes_FRF), 'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     subplot(2,1,2); semilogx(w, imag(Hdelay_removed), 'k', w, imag(total_modeled_modes_FRF), 'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     
%     figure(12); clf; zoom on;
%     plot(real(Hdelay_removed), imag(Hdelay_removed), 'k', real(total_modeled_modes_FRF), imag(total_modeled_modes_FRF), 'b.');
% 
% end

k = 1;
for  i = 1:Nm
    for j = 1:Nn 
% construct the fitted transfer function %
Hremfit(i,j,:) = squeeze(freqresp(Gremfit(i,j,:),w));

% combine fitted remainder TF with fitted contributions from the modes %
Hrem_plus_modes(i,j,:) = total_modeled_modes_FRF(i,j,:) + Hremfit(i,j,:);                        % reconstructed t.f. with summing contributions due to vibration modes and other dynamics (as partial fractions)
Hrem_plus_mode_with_assumed_delay(i,j,:) = Hrem_plus_modes(i,j,:) .* Hdelay_assumed(i,j,:);      % also, the effect of the assumed delay is incorprated as well

% compute fitting error, and its RMS value in selected frequency range %]
fitting_error(i,j,:) = Hfull(i,j,:) - Hrem_plus_mode_with_assumed_delay(i,j,:);
fitting_error_RMS_selected_freq_range(i,j,:) = sqrt( squeeze(fitting_error(i,j,data_range))' * squeeze(fitting_error(i,j,data_range)) / Nrange );

% compare actual and fitted transfer functions %
    if flag_show_figures
    figure(9); zoom on;
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    title(['G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(total_modeled_modes_FRF(i,j,:))),'b');
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hrem(i,j,:))),'r');
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hremfit(i,j,:))),'g--', f, abs(squeeze(Hrem_plus_modes(i,j,:))),'k.');
    legend( 'measurement with assumed delay removed','fitted modes',...
        'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');
    ylabel('mag [ ]');
%     tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hdelay_removed(i,j,:)))),'k'); hold on;
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(total_modeled_modes_FRF(i,j,:)))),'b');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hrem(i,j,:)))),'r');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hremfit(i,j,:)))),'g--', f, r2d*unwrap(angle(squeeze(Hrem_plus_modes(i,j,:)))),'k.');
    ylabel('pha [deg]'); xlabel('freq [Hz]');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(11);  zoom on;
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(total_modeled_modes_FRF(i,j,:))), 'b');
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hrem(i,j,:))), 'r');
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hremfit(i,j,:))),'g--', f,real(squeeze(Hrem_plus_modes(i,j,:))),'k.');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    title(['Test Data and Fitted TF G' num2str(i) num2str(j)]); ylabel('real [ ]');
    legend( 'measurement with assumed delay removed','fitted modes',...
            'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');

    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(total_modeled_modes_FRF(i,j,:))), 'b');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hrem(i,j,:))), 'r');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hremfit(i,j,:))),'g--', f,imag(squeeze(Hrem_plus_modes(i,j,:))),'k.');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    % compare overall fitted TF with originally 'measured' data %
    figure(1);
    subplot(2*Nm,Nn,k);
    loglog(f, abs(squeeze(Hfull(i,j,:))),'k',  f, abs(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
%     tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    ylabel('mag [ ]'); title(['Bode Plot (log freq and mag axes) G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, r2d*unwrap(angle(squeeze(Hfull(i,j,:)))),'k', f, r2d*unwrap(angle(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:)))), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(2);
    subplot(2*Nm,Nn,k);
    plot(f, abs(squeeze(Hfull(i,j,:))),'k', f, abs(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
%     tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
    ylabel('mag [ ]'); title(['Bode Plot (linear freq and mag axes) G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k+Nn);
    plot(f, r2d*unwrap(angle(squeeze(Hfull(i,j,:)))), 'k', f, r2d*unwrap(angle(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:)))), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(3);
    subplot(2*Nm,Nn,k);
    semilogx(f, real(squeeze(Hfull(i,j,:))), 'k', f, real(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [ ]'); title(['Real & Imaginary Components G' num2str(i) num2str(j)]); 
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, imag(squeeze(Hfull(i,j,:))), 'k', f, imag(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
    legend('meas. dynamics','fitted TF','Location','Best');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(13); zoom on;
    subplot(2*Nm,Nn,k);
    semilogx(f, real(squeeze(fitting_error(i,j,:))),'k', f(data_range),real(squeeze(fitting_error(i,j,data_range))),'b');
    title(['Fitting Error G' num2str(i) num2str(j)]); ylabel('Real [ ]'); legend('Complete Data','Selected Range');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, imag(squeeze(fitting_error(i,j,:))),'k', f(data_range),imag(squeeze(fitting_error(i,j,data_range))),'b');
    ylabel('Imag [ ]'); xlabel('freq [Hz]');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
        end
    k = k + 1; 
    end
    k = 2*Nn+1+(i-1)*Nn*2;
end

if flag_show_figures
   k = 1;
   for  i = 1:Nm
       for j = 1:Nn
    figure(14); zoom on;
    subplot(Nm,Nn,k);
    pzmap(squeeze(Gremfit(i,j,:))); 
    k = k + 1;
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
        end
   end
   suptitle('P-Z Map of Identified Remainder Dynamics (excl. modes)');
end
