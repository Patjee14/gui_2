%% Fit remainder dynamics (containing filtering effects and significantly 
%  damped modes) using the Rational Fraction Polynomial (RFP) method 
kmin = ceil(interp1(f,(1:N)',fmin_global_fit));
kmax = floor(interp1(f,(1:N)',fmax_global_fit));
% kmin = 1;
% kmax = length(w);

data_range = (kmin:kmax)';                                                  % data selection range
Nrange = length(data_range);                                                % number of data points in selected range
g = real(Hrem(data_range));	h = imag(Hrem(data_range));                     % real and imaginary components of 'remainder' FRF (after delay and mode residues have been taken out)
One  = ones(Nrange,1);      Zero = zeros(Nrange,1);                         % arrays of ones and zeros (for constructing the regressor matrix)

w1 = w(data_range);
w_power = w1.^-n;
Phi = [w_power ; Zero];
 
% construct the numerator related terms in the regressor matrix %
for k = 1:m
    w_power = w_power .* w1;
    
    if mod(k,4) == 1
        Phi = [Phi , [Zero ; w_power]];
    end
    
    if mod(k,4) == 2
        Phi = [Phi , [-w_power; Zero]];
    end
    
    if mod(k,4) == 3
        Phi = [Phi , [Zero; -w_power]];
    end
       
    if mod(k,4) == 0
        Phi = [Phi , [w_power; Zero]];
    end
end

w_power = w1.^-n;
Phi = [Phi , [-w_power.*g ; -w_power.*h]];
for k = 1:(n-1)
    
    w_power = w_power .* w1;
    
    if mod(k,4) == 1
        Phi = [Phi , [w_power.*h ; -w_power.*g]];
    end
    
    if mod(k,4) == 2
        Phi = [Phi , [w_power.*g ;  w_power.*h]];
    end
    
    if mod(k,4) == 3
        Phi = [Phi , [-w_power.*h ;  w_power.*g]];
    end
        
    if mod(k,4) == 0
        Phi = [Phi , [-w_power.*g ; -w_power.*h]];
    end
    
end

% construct output vector %
Y = [real(j^n*g + j^(n+1)*h) ; imag(j^n*g + j^(n+1)*h)];

% Basic LS - identify transfer function parameters %
% theta = pinv(Phi)*Y;

% weighted LS %
% W = eye(2*Nrange);                                                          % do not use any weights
% W = diag([1./abs(Hrem(data_range)) ; 1./abs(Hrem(data_range))]);                                  % use reciprocal of measured TF amplitude as weighting function
% W = diag([1./(abs(Hrem(data_range)).*abs(Hrem(data_range))) ; 1./(abs(Hrem(data_range)).*abs(Hrem(data_range)))]);        % use square of reciprocal of measured TF amplitude as weighting function
% theta = inv(Phi'*W*Phi)*Phi'*W*Y;                                           % solve parameters

% no weighting %
theta = pinv(Phi)*Y;

% construct numerator coefficients %
b_recip_fit = theta(1:(m+1));                                               % numerator as b0' + b1'*s^1 + b2'*s^2 + ... + bm'*s^m
a_recip_fit = theta((m+2):(m+n+1));                                         % denominator as a0' + a1'*s^1 + a2'*s^2 + ... + a^n
% a_recip_fit = theta((m+2):length(theta));                                   % denominator as a0' + a1'*s^1 + a2'*s^2 + ... + a^n
b_fit = (flipud(b_recip_fit))';                                             % numerator as b0*s^m + b1*s^{m-1} + ... + bm
a_fit = ([1 ; flipud(a_recip_fit)])';                                       % denominator as s^n + a1*s^{n-1} + ... + an

% construct the fitted transfer function %
Gremfit = tf([b_fit],[a_fit]);
Hremfit = squeeze(freqresp(Gremfit,w));

% combine fitted remainder TF with fitted contributions from the modes %
Hrem_plus_modes = total_modeled_modes_FRF + Hremfit;                        % reconstructed t.f. with summing contributions due to vibration modes and other dynamics (as partial fractions)
Hrem_plus_mode_with_assumed_delay = Hrem_plus_modes .* Hdelay_assumed;      % also, the effect of the assumed delay is incorprated as well

% compute fitting error, and its RMS value in selected frequency range %]
fitting_error = Hfull - Hrem_plus_mode_with_assumed_delay;
fitting_error_RMS_selected_freq_range = sqrt( fitting_error(data_range)' * fitting_error(data_range) / Nrange );

% compare actual and fitted transfer functions %
% compare actual and fitted transfer functions %
if flag_show_figures 
    
    figure(9); clf; zoom on;

    subplot(2,1,1); loglog(f,abs(Hdelay_removed),'k'); hold on;
    subplot(2,1,1); loglog(f,abs(total_modeled_modes_FRF),'b');
    subplot(2,1,1); loglog(f,abs(Hrem),'r');
    subplot(2,1,1); loglog(f, abs(Hremfit),'g--', f, abs(Hrem_plus_modes),'k.');
    legend( 'measurement with assumed delay removed','fitted modes',...
        'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');
    ylabel('mag [ ]');
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hdelay_removed)),'k'); hold on;
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(total_modeled_modes_FRF)),'b');
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hrem)),'r');
    subplot(2,1,2); semilogx(f, r2d*unwrap(angle(Hremfit)),'g--', f, r2d*unwrap(angle(Hrem_plus_modes)),'k.');
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    tmp = axis; axis([fview_min fview_max -1000 200]);
    
    figure(11); clf; zoom on;
    subplot(2,1,1); semilogx(f,real(Hdelay_removed),'k'); hold on;
    subplot(2,1,1); semilogx(f, real(total_modeled_modes_FRF), 'b');
    subplot(2,1,1); semilogx(f, real(Hrem), 'r');
    subplot(2,1,1); semilogx(f,real(Hremfit),'g--', f,real(Hrem_plus_modes),'k.');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    title('Test Data and Fitted TF'); ylabel('real [ ]');
    legend( 'measurement with assumed delay removed','fitted modes',...
            'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');

    subplot(2,1,2); semilogx(f,imag(Hdelay_removed),'k'); hold on;
    subplot(2,1,2); semilogx(f, imag(total_modeled_modes_FRF), 'b');
    subplot(2,1,2); semilogx(f, imag(Hrem), 'r');
    subplot(2,1,2); semilogx(f,imag(Hremfit),'g--', f,imag(Hrem_plus_modes),'k.');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    
    % compare overall fitted TF with originally 'measured' data %
    figure(1);
    subplot(2,1,1);
    loglog(f, abs(Hfull),'k',  f, abs(Hrem_plus_mode_with_assumed_delay), 'm--');
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    ylabel('mag [ ]'); title('Bode Plot (log freq and mag axes)');
    subplot(2,1,2);
    semilogx(f, r2d*unwrap(angle(Hfull)),'k', f, r2d*unwrap(angle(Hrem_plus_mode_with_assumed_delay)), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
    tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    
    figure(2);
    subplot(2,1,1);
    plot(f, abs(Hfull),'k', f, abs(Hrem_plus_mode_with_assumed_delay), 'm--');
    tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
    ylabel('mag [ ]'); title('Bode Plot (linear freq and mag axes)');
    subplot(2,1,2);
    plot(f, r2d*unwrap(angle(Hfull)), 'k', f, r2d*unwrap(angle(Hrem_plus_mode_with_assumed_delay)), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
    tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    
    figure(3);
    subplot(2,1,1);
    semilogx(f, real(Hfull), 'k', f, real(Hrem_plus_mode_with_assumed_delay), 'm--');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [ ]'); title('Real & Imaginary Components');
    subplot(2,1,2);
    semilogx(f, imag(Hfull), 'k', f, imag(Hrem_plus_mode_with_assumed_delay), 'm--');
    legend('meas. dynamics','fitted TF','Location','Best');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');

    figure(13); clf; zoom on;
    subplot(2,1,1);
    semilogx(f, real(fitting_error),'k', f(data_range),real(fitting_error(data_range)),'b');
    title('Fitting Error'); ylabel('Real [ ]'); legend('Complete Data','Selected Range');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    subplot(2,1,2);
    semilogx(f, imag(fitting_error),'k', f(data_range),imag(fitting_error(data_range)),'b');
    ylabel('Imag [ ]'); xlabel('freq [Hz]');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    
    figure(14); clf; zoom on;
    pzmap(Gremfit); title('P-Z Map of Identified Remainder Dynamics (excl. modes)');

end
