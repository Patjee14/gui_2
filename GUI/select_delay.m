Hfull = evalin('base','Hfull');
f = evalin('base','f');   
r2d = 180/pi;

% show the bode plot (using linear frequency and magnitude axes) %
figure('Name','Choose the delay','pos',[100 100 870 375]); clf; zoom on;
plot(f,r2d*unwrap(angle(Hfull)),'b'); grid on; hold on;
ylabel('pha [deg]'); title('Bode Plot (linear freq and mag axes)');
xlabel('freq [Hz]');
% legend('First select the minimum frequency and afterwards select the maximum frequency','Location','southoutside'); 
    
% [Xlin,Ylin] = ginput(2);
%  Xlin = round(Xlin);
%  fview_min = Xlin(1);
%  fview_max = Xlin(2);
%  close;

% Assign all the values you need to workspace for the appdesigner


