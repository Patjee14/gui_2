% Load all the data from workspace to use appdesigner
Nmode = evalin('base','Nmode');
fmin = evalin('base','fmin');
fmax = evalin('base','fmax');
fview_min = evalin('base','fview_min');
fview_max = evalin('base','fview_max');
N = evalin('base','N');
f = evalin('base','f');
Hfull = evalin('base','Hfull');
view_data_range = evalin('base','view_data_range');
tf = evalin('base','tf');

%% compute mode indicator function 
MIF = Hfull .* conj(Hfull);
MIF_max = max(MIF(view_data_range));                                        % max and min values within vieweing range
MIF_norm = (1/MIF_max)*MIF;

%% Shows MIF with chosen modes
   
    figure(15); clf; zoom on;
    plot(f,MIF_norm,'k'); grid on; hold on;
    title('Normalized Mode Indicator Function'); ylabel('MIF [%]'); xlabel('freq [Hz]');
    axis([fview_min , fview_max , 0 1]);

   
for kmode = 1:Nmode
        
    if tf(kmode) == 1
    
        % record indices and display selection data %
        % ========================================= %
        kmin(kmode) = ceil(interp1(f,(1:N)',fmin(kmode)));
        kmax(kmode) = floor(interp1(f,(1:N)',fmax(kmode)));
        
        data_range = (kmin(kmode):kmax(kmode))';
        
        figure(15);
        plot(f(data_range),MIF_norm(data_range),'b');
    end  
end