    Hfull = evalin('base','Hfull');
    f = evalin('base','f'); 
    Nm = evalin('base','Nm');
    Nn = evalin('base','Nn');
     
    % show the bode plot (using linear frequency and magnitude axes) %
    k = 1;
    figure('Name','Choose logarithmic magnitude range','pos',[100 50 900 700]); clf; zoom on;
    for i = 1:Nm
        for j = 1:Nn
    subplot(Nm,Nn,k);
    loglog(f,abs(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
    ylabel('mag [ ]');
    xlabel('freq [Hz]');
    k = k + 1;
        end
    end
    legend('First select the minimum magnitude and afterwards select the maximum magnitude','Location','southoutside'); 
    suptitle('Bode Plot (log freq and mag axes)');

    [Xlin,Ylin] = ginput(2);
    Ylin = round(Ylin,3);
    mag_log_view_min = Ylin(1);
    mag_log_view_max = Ylin(2);
    close;
    
    assignin('base','mag_log_view_min',mag_log_view_min);
    assignin('base','mag_log_view_max',mag_log_view_max);
  