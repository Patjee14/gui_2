This text file cotains some comments or possible improvements for the GUI for SISO and MIMO my by Patrick Bevers

Remarks:
- For both SISO and MIMO the input needs to be a .frf file with (time, real values, imaginary values) of the plant. It should be listed in this order!
- For SISO everything works exept the final figures at the end. Not all can be displayed at the same time (up to 16 figures). However this is improve in MATLAB 2017. Also zooming is not an option in this app designer for matlab 2016, but again avaible in matlab 2017 (possible improvements). The y-axis cannot be set according to the given values, this is also not possible in future matlab version. Finnaly, the PZ map is not possible in matlab 2016b or futher. 
- You cannot set the begin figures in MIMO_all_variables, because you cannot make all these figures in the app when running the app. You need to define and make the figures beforehand. (An option could be to make a lot of different apps with figure sizes of 2x2 3x2 3x1 etc. But this would take a lot of space and time). Same hold for displaying the figures after some computation is done.


Improvements:
- Results the nonlinear values can be better displayed
- An nyquist and PZ map needs to be added, see above remarks!
- Add option in the GUI that you can choose between a stable and proper solution (code needs a little bit of modification)
- Fix the display of figures as described above. It is currently not in the app.

If you have any question regarding the code of GUI, please contact me:

Patrick Bevers BSc. 
P.p.f.Bevers@student.tue.nl
Patbevers@gmail.com
+31647296419