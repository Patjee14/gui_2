%% interactively browse through different solution
load(evalin('base','fitting_results'));
log_smallest_RMS_error = min(min(min(log10(fitting_RMS_error_array))));
log_largest_RMS_error  = max(max(max(log10(fitting_RMS_error_array))));

k_delay = evalin('base','k_delay');
k_n = evalin('base','k_n');
k_m = evalin('base','k_m');

    fitting_RMS_error = fitting_RMS_error_array(k_delay,k_n,k_m);
    
    % graphically show where the fit is, in terms of optimality %
    figure(30); clf; zoom on;
    contourf(n_array, m_array, log10(squeeze(fitting_RMS_error_array(k_delay,:,:)))'); hold on;
    caxis([log_smallest_RMS_error , log_largest_RMS_error]); view(0,90); colorbar;
    title(['Log10 of model fitting RMS error (Tdelay = ' num2str(Tdelay_assumed_array(k_delay)) ' [s])' ] );
    xlabel('Remaining dyn. denominator order: n [ ]');     ylabel('Remaining dyn. numerator order: m [ ]');
    grid on;

    plot(n_array(k_n), m_array(k_m) , 'ro'); plot(n_array(k_n), m_array(k_m) , 'yx');     
    text_handle = text(n_array(k_n), m_array(k_m), [num2str(log10(fitting_RMS_error))]);
    set(text_handle,'Color',[1 1 1]);
    disp(['log10 of RMS fitting error: ' num2str(log10(fitting_RMS_error))]);
    
    tmp_choice = ginput(1);
    k_n = round(tmp_choice(1))+1;
    k_m = round(tmp_choice(2))+1;
        
    % reconstruct fitting parameters and required graphs %
    Tdelay_assumed = Tdelay_assumed_array(k_delay);
    n = n_array (k_n); m = m_array (k_m);

    disp('delay , den_order , num_order');
    [Tdelay_assumed n m]
        
    flag_show_figures = 0;
    
    remove_assumed_delay_from_frf_MIMO;                                            % remove delay and show TF
    fit_vibration_modes_compute_remainder_frf_MIMO;
    fit_remaining_dynamics_note_error_MIMO;
    readjust_coefficients_note_error_MIMO;
    
    assignin('base','n',n);
    assignin('base','m',m);
    assignin('base','k_n',k_n);
    assignin('base','k_m',k_m);
        