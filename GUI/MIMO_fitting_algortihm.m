% Load all the data from workspace to use appdesigner
Nmode = evalin('base','Nmode');
fmin = evalin('base','fmin');
fmax = evalin('base','fmax');
Ndelay_assumed = evalin('base','Ndelay_assumed');
N_n = evalin('base','N_n');
N_m = evalin('base','N_m');
n_array = evalin('base','n_array');
m_array = evalin('base','m_array');
Tdelay_assumed_array = evalin('base','Tdelay_assumed_array');
fmin_global_fit = evalin('base','fmin_global_fit');
fmax_global_fit = evalin('base','fmax_global_fit');
mag_log_view_max = evalin('base','mag_log_view_max');
mag_log_view_min = evalin('base','mag_log_view_min');
mag_view_max = evalin('base','mag_view_max');
show_final_figures = evalin('base','show_final_figures');

%% actual fitting and search sequence
%  stepping through the given array of possible delays and remainder
%  dynamics denominator and numerator orders

fitting_RMS_error_array = zeros(Ndelay_assumed,N_n,N_m);                    % storage for curve fitting RMS values evaluated in each case
fitting_RMS_error_log = zeros(Ndelay_assumed*N_n*N_m,4);

flag_show_figures = 0;

% define_plant_frf01;                                                       % define plant FRF
load_plant_test;                                                           % load plant FRF from measurement data

% test different delay values and transfer function polynomial orders
% note the success in curve fitting
log_index = 0;
for k_delay = 1:Ndelay_assumed                                             % counter for tested delay values
    
    Tdelay_assumed = Tdelay_assumed_array(k_delay);
    
    for k_n = 1:N_n                                                        % counter for tested demonimator order for fitting remaining dynamics (after fitting the modes)
        
        n = n_array (k_n);
        
        for k_m = 1:N_m                                                    % counter for tested numerator order for fitting remaining dynamics (after fitting the modes)
        
            m = m_array (k_m);
        
            remove_assumed_delay_from_frf_MIMO;                                           % remove delay and show TF

            fit_vibration_modes_compute_remainder_frf_MIMO;
            
            % fit_remaining_dynamics_note_error01;
            fit_remaining_dynamics_note_error_MIMO;
            readjust_coefficients_note_error_MIMO;

            % note the error obtained in this case %
            fitting_RMS_error_array(k_delay,k_n,k_m) = norm(fitting_error_RMS_selected_freq_range);
            log_index = log_index + 1;
            fitting_RMS_error_log(log_index,:) = [norm(fitting_error_RMS_selected_freq_range) , k_delay, k_n, k_m];

            % echo the tested parameters %
            [Tdelay_assumed , n , m]
           
        end
        
    end
    
end

MIMO_save_and_assign;

% compare actual and fitted transfer functions %
if  show_final_figures
   k = 1;
   for  i = 1:Nm
       for j = 1:Nn
    figure(9); zoom on;
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    title(['G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(total_modeled_modes_FRF(i,j,:))),'b');
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hrem(i,j,:))),'r');
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hremfit(i,j,:))),'g--', f, abs(squeeze(Hrem_plus_modes(i,j,:))),'k.');
    legend( 'measurement with assumed delay removed','fitted modes',...
        'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');
    ylabel('mag [ ]');
    axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hdelay_removed(i,j,:)))),'k'); hold on;
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(total_modeled_modes_FRF(i,j,:)))),'b');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hrem(i,j,:)))),'r');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hremfit(i,j,:)))),'g--', f, r2d*unwrap(angle(squeeze(Hrem_plus_modes(i,j,:)))),'k.');
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    axis([fview_min fview_max -1000 200]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(11);  zoom on;
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(total_modeled_modes_FRF(i,j,:))), 'b');
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hrem(i,j,:))), 'r');
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hremfit(i,j,:))),'g--', f,real(squeeze(Hrem_plus_modes(i,j,:))),'k.');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    title(['Test Data and Fitted TF G' num2str(i) num2str(j)]); ylabel('real [ ]');
    legend( 'measurement with assumed delay removed','fitted modes',...
            'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');

    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(total_modeled_modes_FRF(i,j,:))), 'b');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hrem(i,j,:))), 'r');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hremfit(i,j,:))),'g--', f,imag(squeeze(Hrem_plus_modes(i,j,:))),'k.');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    % compare overall fitted TF with originally 'measured' data %
    figure(1);
    subplot(2*Nm,Nn,k);
    loglog(f, abs(squeeze(Hfull(i,j,:))),'k',  f, abs(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
    axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    ylabel('mag [ ]'); title(['Bode Plot (log freq and mag axes) G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, r2d*unwrap(angle(squeeze(Hfull(i,j,:)))),'k', f, r2d*unwrap(angle(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:)))), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
    axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(2);
    subplot(2*Nm,Nn,k);
    plot(f, abs(squeeze(Hfull(i,j,:))),'k', f, abs(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
    axis([fview_min fview_max 0 mag_view_max]);
    ylabel('mag [ ]'); title(['Bode Plot (linear freq and mag axes) G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k+Nn);
    plot(f, r2d*unwrap(angle(squeeze(Hfull(i,j,:)))), 'k', f, r2d*unwrap(angle(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:)))), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
	axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(3);
    subplot(2*Nm,Nn,k);
    semilogx(f, real(squeeze(Hfull(i,j,:))), 'k', f, real(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [ ]'); title(['Real & Imaginary Components G' num2str(i) num2str(j)]); 
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, imag(squeeze(Hfull(i,j,:))), 'k', f, imag(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
    legend('meas. dynamics','fitted TF','Location','Best');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(13); zoom on;
    subplot(2*Nm,Nn,k);
    semilogx(f, real(squeeze(fitting_error(i,j,:))),'k', f(data_range),real(squeeze(fitting_error(i,j,data_range))),'b');
    title(['Fitting Error G' num2str(i) num2str(j)]); ylabel('Real [ ]'); legend('Complete Data','Selected Range');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, imag(squeeze(fitting_error(i,j,:))),'k', f(data_range),imag(squeeze(fitting_error(i,j,data_range))),'b');
    ylabel('Imag [ ]'); xlabel('freq [Hz]');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
       k = k + 1; 
       end
       k = 2*Nn+1+(i-1)*Nn*2; 
    end
end

if show_final_figures
   k = 1;
   for  i = 1:Nm
       for j = 1:Nn
    figure(14); zoom on;
    subplot(Nm,Nn,k);
    pzmap(squeeze(Gremfit(i,j,:))); 
    k = k + 1;
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
        end
   end
   suptitle('P-Z Map of Identified Remainder Dynamics (excl. modes)');
end