%% Fit remainder dynamics (containing filtering effects and significantly 
%  damped modes) using the Rational Fraction Polynomial (RFP) method 

kmin = ceil(interp1(f,(1:N)',fmin_global_fit));
kmax = floor(interp1(f,(1:N)',fmax_global_fit));
% kmin = 10;
% kmax = 20;

data_range = (kmin:kmax)';                                                  % data selection range
Nrange = length(data_range);   
g = zeros(Nm,Nn,Nrange);
h = zeros(Nm,Nn,Nrange);
One  = ones(Nrange,1);      Zero = zeros(Nrange,1);                         % arrays of ones and zeros (for constructing the regressor matrix)

w1 = w(data_range);
w_power = w1.^-n;
Phi = zeros(2*Nm*Nn*Nrange,Nm*Nn);

for qb = 1:Nrange
    for q = 1:Nm*Nn             % to fill the first part of the regresson matrix
    Phi(qb+2*Nrange*(q-1),q) = w_power(qb);
    end
end

% construct the numerator related terms in the regressor matrix %
for k = 1:m
    w_power = w_power .* w1;
    Phib = zeros(2*Nm*Nn*Nrange,Nm*Nn);
    if mod(k,4) == 1
        for qb = 1:Nrange
            for q = 1:Nm*Nn            
         Phib(qb+Nrange+2*Nrange*(q-1),q) = w_power(qb);
            end
        end
        Phi = [Phi , Phib];
    end
    
    if mod(k,4) == 2
        for qb = 1:Nrange
            for q = 1:Nm*Nn           
         Phib(qb+2*Nrange*(q-1),q) = -w_power(qb);
            end
        end
        Phi = [Phi , Phib];
    end
    
    if mod(k,4) == 3
        for qb = 1:Nrange
            for q = 1:Nm*Nn            
         Phib(qb+Nrange+2*Nrange*(q-1),q) = -w_power(qb);
            end
        end
        Phi = [Phi , Phib];
    end
        
    if mod(k,4) == 0
        for qb = 1:Nrange
            for q = 1:Nm*Nn            
         Phib(qb+2*Nrange*(q-1),q) = w_power(qb);
            end
        end
        Phi = [Phi , Phib];
    end
end

w_power = w1.^-n;
Phia = [];
Y = [];
for  i = 1:Nm
    for j = 1:Nn % number of data points in selected range
    g(i,j,:) = real(Hrem(i,j,data_range));	h(i,j,:) = imag(Hrem(i,j,data_range));                     % real and imaginary components of 'remainder' FRF (after delay and mode residues have been taken out)
    Phia = [Phia; [-w_power.*squeeze(g(i,j,:)) ; -w_power.*squeeze(h(i,j,:))]];
    
    % construct output vector %
    Y = [Y;[real(1i^n*squeeze(g(i,j,:)) + 1i^(n+1)*squeeze(h(i,j,:))) ; imag(1i^n*squeeze(g(i,j,:)) + 1i^(n+1)*squeeze(h(i,j,:)))]];
    end
end
Phi = [Phi , Phia];

for k = 1:(n-1)
    Phia = [];
    w_power = w_power .* w1;
    
    if mod(k,4) == 1
        for  i = 1:Nm
            for j = 1:Nn % number of data points in selected range
    Phia = [Phia; [w_power.*squeeze(h(i,j,:)); -w_power.*squeeze(g(i,j,:))]];
            end
        end
        Phi = [Phi , Phia];
    end
    
    if mod(k,4) == 2
        for  i = 1:Nm
            for j = 1:Nn % number of data points in selected range
    Phia = [Phia; [w_power.*squeeze(g(i,j,:)) ; w_power.*squeeze(h(i,j,:))]];
            end
        end
        Phi = [Phi , Phia];
    end
    
    if mod(k,4) == 3
        for  i = 1:Nm
            for j = 1:Nn % number of data points in selected range
    Phia = [Phia; [-w_power.*squeeze(h(i,j,:)); w_power.*squeeze(g(i,j,:))]];
            end
        end
        Phi = [Phi , Phia];
    end
        
    if mod(k,4) == 0
        for  i = 1:Nm
            for j = 1:Nn % number of data points in selected range
    Phia = [Phia; [-w_power.*squeeze(g(i,j,:)) ; -w_power.*squeeze(h(i,j,:))]];
            end
        end
        Phi = [Phi , Phia];
    end
    
end

% Basic LS - identify transfer function parameters %
% B = pinv(Phi)
% theta = Phi\Y;
% theta2 = B*Y

% weighted LS %
% W = eye(2*Nrange);                                                          % do not use any weights
% W = diag([1./abs(squeeze(Hrem(i,j,data_range))) ; 1./abs(squeeze(Hrem(i,j,data_range)))]);                                  % use reciprocal of measured TF amplitude as weighting function
% W = diag([1./(abs(squeeze(Hrem(i,j,data_range))).*abs(squeeze(Hrem(i,j,data_range)))) ; 1./(abs(squeeze(Hrem(i,j,data_range)))).*abs(squeeze(Hrem(i,j,data_range)))]);        % use square of reciprocal of measured TF amplitude as weighting function
% theta = inv(Phi'*W*Phi)*Phi'*W*Y;                                           % solve parameters

% no weighting %
theta = pinv(Phi)*Y;

b_recip_fit = [];
q = 1;
b_fit = zeros(Nm,Nn,m+1);
for  i = 1:Nm
    for j = 1:Nn
        for k = 1:(m+1)
    % construct numerator coefficients %
    b_recip_fit = [b_recip_fit, theta(q+Nm*Nn*(k-1))];                                               % numerator as b0' + b1'*s^1 + b2'*s^2 + ... + bm'*s^m
        end
    b_fit(i,j,:) = (flipud(b_recip_fit')); 
    b_recip_fit = [];
    q = q + 1;
    end
end

a_recip_fit = theta(((m+1)*Nm*Nn)+1:(Nm*Nn*(m+1)+n));                                         % denominator as a0' + a1'*s^1 + a2'*s^2 + ... + a^n
a_fit = ([1 ; flipud(a_recip_fit)])';                                       % denominator as s^n + a1*s^{n-1} + ... + an
k = 1;

for  i = 1:Nm
    for j = 1:Nn
% construct the fitted transfer function %
Gremfit(i,j,:) = tf([squeeze(b_fit(i,j,:))'],[a_fit]);
Hremfit(i,j,:) = squeeze(freqresp(Gremfit(i,j,:),w));
 
% combine fitted remainder TF with fitted contributions from the modes %
Hrem_plus_modes(i,j,:) = total_modeled_modes_FRF(i,j,:) + Hremfit(i,j,:);                        % reconstructed t.f. with summing contributions due to vibration modes and other dynamics (as partial fractions)
Hrem_plus_mode_with_assumed_delay(i,j,:) = Hrem_plus_modes(i,j,:) .* Hdelay_assumed(i,j,:);      % also, the effect of the assumed delay is incorprated as well

% compute fitting error, and its RMS value in selected frequency range %]
fitting_error(i,j,:) = Hfull(i,j,:) - Hrem_plus_mode_with_assumed_delay(i,j,:);
fitting_error_RMS_selected_freq_range(i,j,:) = sqrt( squeeze(fitting_error(i,j,data_range))' * squeeze(fitting_error(i,j,data_range)) / Nrange );

% compare actual and fitted transfer functions %
% compare actual and fitted transfer functions %
    if flag_show_figures
    figure(9); zoom on;
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    title(['G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(total_modeled_modes_FRF(i,j,:))),'b');
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hrem(i,j,:))),'r');
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hremfit(i,j,:))),'g--', f, abs(squeeze(Hrem_plus_modes(i,j,:))),'k.');
    legend( 'measurement with assumed delay removed','fitted modes',...
        'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');
    ylabel('mag [ ]');
%     tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hdelay_removed(i,j,:)))),'k'); hold on;
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(total_modeled_modes_FRF(i,j,:)))),'b');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hrem(i,j,:)))),'r');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hremfit(i,j,:)))),'g--', f, r2d*unwrap(angle(squeeze(Hrem_plus_modes(i,j,:)))),'k.');
    ylabel('pha [deg]'); xlabel('freq [Hz]');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(11);  zoom on;
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(total_modeled_modes_FRF(i,j,:))), 'b');
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hrem(i,j,:))), 'r');
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hremfit(i,j,:))),'g--', f,real(squeeze(Hrem_plus_modes(i,j,:))),'k.');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    title(['Test Data and Fitted TF G' num2str(i) num2str(j)]); ylabel('real [ ]');
    legend( 'measurement with assumed delay removed','fitted modes',...
            'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');

    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(total_modeled_modes_FRF(i,j,:))), 'b');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hrem(i,j,:))), 'r');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hremfit(i,j,:))),'g--', f,imag(squeeze(Hrem_plus_modes(i,j,:))),'k.');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    % compare overall fitted TF with originally 'measured' data %
    figure(1);
    subplot(2*Nm,Nn,k);
    loglog(f, abs(squeeze(Hfull(i,j,:))),'k',  f, abs(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
%     tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    ylabel('mag [ ]'); title(['Bode Plot (log freq and mag axes) G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, r2d*unwrap(angle(squeeze(Hfull(i,j,:)))),'k', f, r2d*unwrap(angle(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:)))), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(2);
    subplot(2*Nm,Nn,k);
    plot(f, abs(squeeze(Hfull(i,j,:))),'k', f, abs(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
%     tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
    ylabel('mag [ ]'); title(['Bode Plot (linear freq and mag axes) G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k+Nn);
    plot(f, r2d*unwrap(angle(squeeze(Hfull(i,j,:)))), 'k', f, r2d*unwrap(angle(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:)))), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(3);
    subplot(2*Nm,Nn,k);
    semilogx(f, real(squeeze(Hfull(i,j,:))), 'k', f, real(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [ ]'); title(['Real & Imaginary Components G' num2str(i) num2str(j)]); 
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, imag(squeeze(Hfull(i,j,:))), 'k', f, imag(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
    legend('meas. dynamics','fitted TF','Location','Best');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(13); zoom on;
    subplot(2*Nm,Nn,k);
    semilogx(f, real(squeeze(fitting_error(i,j,:))),'k', f(data_range),real(squeeze(fitting_error(i,j,data_range))),'b');
    title(['Fitting Error G' num2str(i) num2str(j)]); ylabel('Real [ ]'); legend('Complete Data','Selected Range');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, imag(squeeze(fitting_error(i,j,:))),'k', f(data_range),imag(squeeze(fitting_error(i,j,data_range))),'b');
    ylabel('Imag [ ]'); xlabel('freq [Hz]');
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
        end
    k = k + 1; 
    end
    k = 2*Nn+1+(i-1)*Nn*2; 
end

if flag_show_figures
   k = 1;
   for  i = 1:Nm
       for j = 1:Nn
    figure(14); zoom on;
    subplot(Nm,Nn,k);
    pzmap(squeeze(Gremfit(i,j,:))); 
    k = k + 1;
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
        end
   end
   suptitle('P-Z Map of Identified Remainder Dynamics (excl. modes)');
end
  

