% load data file %
value_input = evalin('base','value_input');
value_output =  evalin('base','value_output');

for i = 1:value_output
    for j = 1:value_input
input_fname = evalin('base',['input_fname',num2str(i),num2str(j)]);
tmp = load(input_fname);

%% To extract the transfer functions from the measurements structure.
f = tmp(:,1);
N = length(f);
y1 = tmp(:,2);
y2 = tmp(:,3);

w = 2*pi*f;
jw = 1i*2*pi*f;
r2d = 180/pi;

% Accelerometer Acceleration
Acc_R = y1; %%Real
Acc_I = y2; %%Imaginary
Acc_M=abs(Acc_R+sqrt(-1)*Acc_I);             %%Magnitude
Acc_P=unwrap(angle(Acc_R+sqrt(-1)*Acc_I));   %%Phase

G_Acc_Measurement = Acc_R+Acc_I*1i;
G(i,j) = frd(G_Acc_Measurement,w);
Hfull = G.ResponseData;

    end
end
% choose the a1vr transfer function to fit %
Nn = length(Hfull(1,:,1));
Nm = length(Hfull(:,1,1));

%calculation so the app can display the MIF
for i = 1:Nm
    for j = 1:Nn
    MIF(i,j,:) = Hfull(i,j,:) .* conj(Hfull(i,j,:));
    MIF_max(i,j,:) = max(MIF(i,j,:));                
    MIF_norm(i,j,:) = (1/MIF_max(i,j,:))*MIF(i,j,:);
    end
end

% Assign all the values you need to workspace for the appdesigner
assignin('base','N',N);
assignin('base','f',f);
assignin('base','Hfull',Hfull);
assignin('base','Nm',Nm);
assignin('base','Nn',Nn);
assignin('base','MIF_norm',MIF_norm);
assignin('base','MIF',MIF);