% m file to load exprimental FRF %

% load data file %
tmp = load('cstsystem.mat')

%% To extract the transfer functions from the measurements structure.
w = tmp.G_frd.Frequency;
N = length(w);
Nn = length(tmp.G_frd(1,:));
Nm = length(tmp.G_frd(:,1));
f = w/(2*pi);
jw = 1i*w;
r2d = 180/pi;

Hfull = tmp.G_frd.ResponseData;

% for computing peak value of MIF, choose data range 
% consistent with viewing frequency range
kmin = ceil(interp1(f,(1:N)',fview_min));
kmax = floor(interp1(f,(1:N)',fview_max));
view_data_range = kmin:kmax;

%% visualization 

if flag_show_figures
    
    % show the bode plot (using logarithmic frequency and magnitude axes) %
    k = 1;
    for  i = 1:Nn
        for j = 1:Nm
    figure(1); zoom on;
    subplot(2*Nn,Nm,k); loglog(f,abs(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
    ylabel('mag [ ]'); title(['Bode Plot (log freq and mag axes) G' num2str(i) num2str(j)]);
    legend('meas. dynamics (including delay)','Location','Best');
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    subplot(2*Nn,Nm,k+2); semilogx(f,r2d*unwrap(angle(squeeze(Hfull(i,j,:)))),'b'); grid on; hold on;
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    axis([fview_min fview_max -1000 200]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    % show the bode plot (using linear frequency and magnitude axes) %
    figure(2); zoom on;
    subplot(2*Nn,Nm,k); plot(f,abs(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
    axis([fview_min fview_max tmp(3) mag_view_max]);
    ylabel('mag [ ]'); title(['Bode Plot (linear freq and mag axes) G' num2str(i) num2str(j)]);
    legend('meas. dynamics (including delay)','Location','Best');
    subplot(2*Nn,Nm,k+2); plot(f,r2d*unwrap(angle(squeeze(Hfull(i,j,:)))),'b'); grid on; hold on;
    axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    % show the real and imaginary components using logarithmic frequency axes %
    figure(3); zoom on;               
    subplot(2*Nn,Nm,k); semilogx(f,real(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
	axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [ ]'); title(['Real & Imaginary Components G' num2str(i) num2str(j)]);
    legend('meas. dynamics (including delay)','Location','Best');
    subplot(2*Nn,Nm,k+2); semilogx(f,imag(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
	axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    k = k + 1; 
        end
    k = 2*Nn+1;
    end
    
    % show the real-imag components using a Nyquist plot %
    figure(4); clf; zoom on;
    k = 1;
    for  i = 1:Nn
        for j = 1:Nm
    subplot(Nn,Nm,k);
    plot(real(squeeze(Hfull(i,j,view_data_range))),imag(squeeze(Hfull(i,j,view_data_range))),'b'); grid on; hold on;
    xlabel('real [ ]'); ylabel('imag [ ]'); title(['Real & Imaginary Components (selected viewing data range) G' num2str(i) num2str(j)]);
    axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
    legend('meas. dynamics (including delay)','Location','Best');
    k = k + 1; 
        end
    end
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
end