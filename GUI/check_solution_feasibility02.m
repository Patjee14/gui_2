function [flag_feasible] = check_solution_feasibility02(theta_normalized, other_parameters)

% ======================================================================= %
% m function to check the stability and distinctness of the poles in the  %
% proposed normalized optimization parameter vector                       % 
% ======================================================================= %

% auxiliary parameters %
optim_var_range = other_parameters.optim_var_range;
wn_range        = other_parameters.wn_range;
zeta_range      = other_parameters.zeta_range;
p_range         = other_parameters.p_range;
Nc = other_parameters.Nc;
Nr = other_parameters.Nr;
Nd = other_parameters.Nd;

% de-normalize optimization variable vector %
theta = optim_var_range.*theta_normalized;

% wn, zeta, and p (real pole) values in the new ordering
wn   = theta(wn_range);                                          % the wn and zeta values (from both the modes and 'remainder' dynamics)
zeta = theta(zeta_range);
p    = theta(p_range);                                           % real pole values

flag_feasible = 1;                                                          % initially assume feasibility

% produce an error if any poles are unstable %
if min(zeta) < 0
    Error_complex_poles;
    flag_feasible = 0;
end

if min(p) < 0
    Error_real_poles;
    flag_feasible = 0;
end

% check the distinctness of the poles %
poles_complex = -zeta.*wn + 1i*wn.*sqrt(1 - zeta.*zeta);                                   % construct complex poles
distinct_poles_complex = unique(poles_complex);
poles_real = -p;
distinct_poles_real    = unique(poles_real);

if length(distinct_poles_complex) ~= Nc
    Error_complex_distinct_poles;
    flag_feasible = 0;
end

if length(distinct_poles_real) ~= Nr
    Error_real_distinct_poles;
    flag_feasible = 0;
end

disp('test');