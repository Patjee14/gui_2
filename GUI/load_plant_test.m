% m file to load exprimental FRF %

% Load all the data from workspace to use appdesigner
input_fname = evalin('base','input_fname');
fview_min = evalin('base','fview_min');
fview_max = evalin('base','fview_max');

% load data file 
tmp = load(input_fname);

%% To extract the transfer functions from the measurements structure.
f = tmp(:,1);
N = length(f);
y1 = tmp(:,2);
y2 = tmp(:,3);

w = 2*pi*f;
jw = 1i*2*pi*f;
r2d = 180/pi;

% Accelerometer Acceleration
Acc_R = y1; %%Real
Acc_I = y2; %%Imaginary
Acc_M=abs(Acc_R+sqrt(-1)*Acc_I);             %%Magnitude
Acc_P=unwrap(angle(Acc_R+sqrt(-1)*Acc_I));   %%Phase

G_Acc_Measurement = Acc_R + 1i*Acc_I;
G = frd(G_Acc_Measurement,w);

% choose the a1vr transfer function to fit %
Hfull = [G.ResponseData G.ResponseData; G.ResponseData G.ResponseData];
Nn = length(Hfull(1,:,1));
Nm = length(Hfull(:,1,1));

% for computing peak value of MIF, choose data range 
% consistent with viewing frequency range
kmin = ceil(interp1(f,(1:N)',fview_min));
kmax = floor(interp1(f,(1:N)',fview_max));
view_data_range = kmin:kmax;

% Assign all the values you need to workspace for the appdesigner
assignin('base','N',N);
assignin('base','f',f);
assignin('base','Hfull',Hfull);
assignin('base','kmin',kmin);
assignin('base','kmax',kmax);
assignin('base','view_data_range',view_data_range);
assignin('base','Nm',Nm);
assignin('base','Nn',Nn);

%% visualization 

if flag_show_figures
    
    % show the bode plot (using logarithmic frequency and magnitude axes) %
    k = 1;
    for  i = 1:Nm
        for j = 1:Nn
    figure(1); zoom on;
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
    ylabel('mag [ ]'); title(['Bode Plot (log freq and mag axes) G' num2str(i) num2str(j)]);
    legend('meas. dynamics (including delay)','Location','Best');
%     tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hfull(i,j,:)))),'b'); grid on; hold on;
    ylabel('pha [deg]'); xlabel('freq [Hz]');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
   
    % show the bode plot (using linear frequency and magnitude axes) %
    figure(2); zoom on;
    subplot(2*Nm,Nn,k); plot(f,abs(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max tmp(3) mag_view_max]);
    ylabel('mag [ ]'); title(['Bode Plot (linear freq and mag axes) G' num2str(i) num2str(j)]);
    legend('meas. dynamics (including delay)','Location','Best');
    subplot(2*Nm,Nn,k+Nn); plot(f,r2d*unwrap(angle(squeeze(Hfull(i,j,:)))),'b'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    % show the real and imaginary components using logarithmic frequency axes %
    figure(3); zoom on;               
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [ ]'); title(['Real & Imaginary Components G' num2str(i) num2str(j)]);
    legend('meas. dynamics (including delay)','Location','Best');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    k = k + 1;
        end
    k = 2*Nn+1+(i-1)*Nn*2; 
    end
    
    % show the real-imag components using a Nyquist plot %
    figure(4); clf; zoom on;
    k = 1;
    for  i = 1:Nm
        for j = 1:Nn
    subplot(Nm,Nn,k);
    plot(real(squeeze(Hfull(i,j,view_data_range))),imag(squeeze(Hfull(i,j,view_data_range))),'b'); grid on; hold on;
    xlabel('real [ ]'); ylabel('imag [ ]'); title(['Real & Imaginary Components (selected viewing data range) G' num2str(i) num2str(j)]);
%     axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
    legend('meas. dynamics (including delay)','Location','Best');
    k = k + 1; 
        end
    end
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
end