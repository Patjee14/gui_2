Hfull = evalin('base','Hfull');
f = evalin('base','f');   
r2d = 180/pi;

% show the bode plot (using linear frequency and magnitude axes) %
k = 1;
figure('Name','Choose the delay','pos',[100 50 900 700]); clf; zoom on;
for i = 1:Nm
    for j = 1:Nn
subplot(Nm,Nn,k);
plot(f,r2d*unwrap(angle(squeeze(Hfull(i,j,:)))),'b'); grid on; hold on;
ylabel('pha [deg]'); 
xlabel('freq [Hz]');
k = k + 1;
    end
end
suptitle('Bode Plot (linear freq and mag axes)');
% legend('First select the minimum frequency and afterwards select the maximum frequency','Location','southoutside'); 
    
% [Xlin,Ylin] = ginput(2);
%  Xlin = round(Xlin);
%  fview_min = Xlin(1);
%  fview_max = Xlin(2);
%  close;

% Assign all the values you need to workspace for the appdesigner
