% load data file %
input_fname = evalin('base','input_fname');
tmp = load(input_fname);

%% To extract the transfer functions from the measurements structure.
f = tmp(:,1);
N = length(f);
y1 = tmp(:,2);
y2 = tmp(:,3);

w = 2*pi*f;
jw = j*2*pi*f;
r2d = 180/pi;

% Accelerometer Acceleration
Acc_R = y1; %%Real
Acc_I = y2; %%Imaginary
Acc_M=abs(Acc_R+sqrt(-1)*Acc_I);             %%Magnitude
Acc_P=unwrap(angle(Acc_R+sqrt(-1)*Acc_I));   %%Phase

G_Acc_Measurement = Acc_R+Acc_I*i;

% choose the a1vr transfer function to fit %
Hfull = G_Acc_Measurement;

% Assign all the values you need to workspace for the appdesigner
assignin('base','N',N);
assignin('base','f',f);
assignin('base','Hfull',Hfull);

%% visualization 
    
    % show the bode plot (using linear frequency and magnitude axes) %
    figure('Name','Choose frequency range'); clf; zoom on;
    subplot(2,1,1); plot(f,abs(Hfull),'b'); grid on; hold on;
    ylabel('mag [ ]'); title('Bode Plot (linear freq and mag axes)');
    subplot(2,1,2); plot(f,r2d*unwrap(angle(Hfull)),'b'); grid on; hold on;
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    legend('meas. dynamics (including delay)','Location','Best');
       
    [Xlin,Ylin] = ginput(2);
    Xlin = round(Xlin);
    fview_min = Xlin(1);
    fview_max = Xlin(2);
    close;
    
    % show the real and imaginary components using logarithmic frequency axes %
    figure('Name','Choose maximum maginute to view'); clf; zoom on;
    subplot(2,1,1); semilogx(f,real(Hfull),'b'); grid on; hold on;
    ylabel('real [ ]'); title('Real & Imaginary Components');
    subplot(2,1,2); semilogx(f,imag(Hfull),'b'); grid on; hold on;
	ylabel('imag [ ]'); xlabel('freq [Hz]');
    legend('meas. dynamics (including delay)','Location','Best');
     
    [Xmag,Ymag] = ginput(1);
    Ymag = round(Ymag);
    mag_view_max = abs(Ymag);
    close;
    
    assignin('base','fview_min',fview_min);
    assignin('base','fview_max',fview_max);
    assignin('base','mag_view_max',mag_view_max);
    
%     % show the bode plot (using logarithmic frequency and magnitude axes) %
%     figure(1); clf; zoom on;
%     subplot(2,1,1); loglog(f,abs(Hfull),'b'); grid on; hold on;
%     ylabel('mag [ ]'); title('Bode Plot (log freq and mag axes)');
%     subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hfull)),'b'); grid on; hold on;
%     ylabel('pha [deg]'); xlabel('freq [Hz]');
%     legend('meas. dynamics (including delay)','Location','Best');
    
%     % show the real-imag components using a Nyquist plot %
%     figure(4); clf; zoom on;
%     plot(real(Hfull(view_data_range)),imag(Hfull(view_data_range)),'b'); grid on; hold on;
%     xlabel('real [ ]'); ylabel('imag [ ]'); title('Real & Imaginary Components (selected viewing data range)');
%     axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
