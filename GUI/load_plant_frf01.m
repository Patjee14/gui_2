% m file to load exprimental FRF %

% Load all the data from workspace to use appdesigner
input_fname = evalin('base','input_fname');
fview_min = evalin('base','fview_min');
fview_max = evalin('base','fview_max');

% load data file %
tmp = load(input_fname);

%% To extract the transfer functions from the measurements structure.
f = tmp(:,1);
N = length(f);
y1 = tmp(:,2);
y2 = tmp(:,3);

w = 2*pi*f;
jw = j*2*pi*f;
r2d = 180/pi;

% Accelerometer Acceleration
Acc_R = y1; %%Real
Acc_I = y2; %%Imaginary
Acc_M=abs(Acc_R+sqrt(-1)*Acc_I);             %%Magnitude
Acc_P=unwrap(angle(Acc_R+sqrt(-1)*Acc_I));   %%Phase

G_Acc_Measurement = Acc_R+Acc_I*i;

% choose the a1vr transfer function to fit %
Hfull = G_Acc_Measurement;

% for computing peak value of MIF, choose data range 
% consistent with viewing frequency range
kmin = ceil(interp1(f,(1:N)',fview_min));
kmax = floor(interp1(f,(1:N)',fview_max));
view_data_range = kmin:kmax;

% Assign all the values you need to workspace for the appdesigner
assignin('base','N',N);
assignin('base','f',f);
assignin('base','Hfull',Hfull);
assignin('base','kmin',kmin);
assignin('base','kmax',kmax);
assignin('base','view_data_range',view_data_range);

%% visualization 
% 
% if flag_show_figures
%     
%     % show the bode plot (using logarithmic frequency and magnitude axes) %
%     figure(1); clf; zoom on;
%     subplot(2,1,1); loglog(f,abs(Hfull),'b'); grid on; hold on;
%     ylabel('mag [ ]'); title('Bode Plot (log freq and mag axes)');
%     tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
%     subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hfull)),'b'); grid on; hold on;
%     ylabel('pha [deg]'); xlabel('freq [Hz]');
%     tmp = axis; axis([fview_min fview_max -1000 200]);
%     legend('meas. dynamics (including delay)','Location','Best');
%     
%     % show the bode plot (using linear frequency and magnitude axes) %
%     figure(2); clf; zoom on;
%     subplot(2,1,1); plot(f,abs(Hfull),'b'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max tmp(3) mag_view_max]);
%     ylabel('mag [ ]'); title('Bode Plot (linear freq and mag axes)');
%     subplot(2,1,2); plot(f,r2d*unwrap(angle(Hfull)),'b'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max -1000 200]);
%     ylabel('pha [deg]'); xlabel('freq [Hz]');
%     legend('meas. dynamics (including delay)','Location','Best');
%     
%     % show the real and imaginary components using logarithmic frequency axes %
%     figure(3); clf; zoom on;
%     subplot(2,1,1); semilogx(f,real(Hfull),'b'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
%     ylabel('real [ ]'); title('Real & Imaginary Components');
%     subplot(2,1,2); semilogx(f,imag(Hfull),'b'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
%     ylabel('imag [ ]'); xlabel('freq [Hz]');
%     legend('meas. dynamics (including delay)','Location','Best');
%     
%     % show the real-imag components using a Nyquist plot %
%     figure(4); clf; zoom on;
%     plot(real(Hfull(view_data_range)),imag(Hfull(view_data_range)),'b'); grid on; hold on;
%     xlabel('real [ ]'); ylabel('imag [ ]'); title('Real & Imaginary Components (selected viewing data range)');
%     axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
%     
% end
