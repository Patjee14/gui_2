% m file to define the plant and generate its FRF %

%% parameters %

% mode #1 %
ws1 = 2*pi*50;              % nat. freq [rad/s]
zs1 = 0.02;                 % damping ratio
k1  = 1e6;                  % stiffness [N/m]

% ws1 = 2*pi*95;              % nat. freq [rad/s]
% zs1 = 0.02;                 % damping ratio
% k1  = 1e6;                  % stiffness [N/m]

% mode #2 %
ws2 = 2*pi*100;              % nat. freq [rad/s]
zs2 = 0.005;                % damping ratio
k2  = 0.5e6;                % stiffness [N/m]

% mode #3 %
ws3 = 2*pi*200;              % nat. freq [rad/s]
zs3 = 0.01;                 % damping ratio
k3  = 0.2e6;                % stiffness [N/m]

% mode #4 %
ws4 = 2*pi*300;              % nat. freq [rad/s]
zs4 = 0.03;                 % damping ratio
k4  = 0.05e6;                % stiffness [N/m]

%% calculations

% linear dynamics, consisting of modes %
G1 =    tf((1/k1)*[ws1*ws1],[1 2*zs1*ws1 ws1*ws1]);
G2 =    -tf((1/k2)*[ws2*ws2],[1 2*zs2*ws2 ws2*ws2]);
G3 =    tf((1/k3)*[ws3*ws3],[1 2*zs3*ws3 ws3*ws3]);
G4 =    tf((1/k4)*[ws4*ws4],[1 2*zs4*ws4 ws4*ws4]);
Gmodes  = G1 + G2 + G3 + G4;

% remaining dynamics (multiplicative) %
Gother = tf([1/50 1],[1/500 1])*tf([1 0],[1/200 1]);
% Gother = tf(1,1);

% combine modes and other dynamics %
Gtest = Gmodes*Gother;

% define delay characteristic %
Tdelay = 4e-3;

% generate FRF %
r2d = 180/pi;

N = 5000;                                                                   % number of frequency points
w = linspace(100,10000,N)';                                                 % frequency vectort [rad/s]

Hmodes  = squeeze(freqresp(Gmodes,w));                                        % FRF of plant without delay
Htest  = squeeze(freqresp(Gtest,w));                                        % FRF of plant without delay
Hdelay = cos(w*Tdelay) - j*sin(w*Tdelay);                                   % FRF of pure delay
Hfull = Htest.*Hdelay;                                                      % FRF of plant including delay

%% visualization 

if flag_show_figures,
    
    % show the bode plot (using logarithmic frequency and magnitude axes) %
    figure(1); clf; zoom on;
    subplot(2,1,1); loglog(w,abs(Htest),'k', w,abs(Hdelay),'c', w,abs(Hfull),'b'); grid on; hold on;
    ylabel('mag [m/N]'); title('Bode Plot (log freq and mag axes)');
    tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
    subplot(2,1,2); semilogx(w,r2d*angle(Htest),'k', w,r2d*angle(Hdelay),'c', w,r2d*angle(Hfull),'b'); grid on; hold on;
    ylabel('pha [deg]'); xlabel('freq [rad/s]');
    legend('dynamics without delay','pure delay','dynamics including delay','Location','Best');
    tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
    
    % show the bode plot (using linear frequency and magnitude axes) %
    figure(2); clf; zoom on;
    subplot(2,1,1); plot(w,abs(Htest),'k', w,abs(Hdelay),'c', w,abs(Hfull),'b'); grid on; hold on;
    tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
    ylabel('mag [m/N]'); title('Bode Plot (linear freq and mag axes)');
    subplot(2,1,2); plot(w,r2d*angle(Htest),'k', w,r2d*angle(Hdelay),'c', w,r2d*angle(Hfull),'b'); grid on; hold on;
    tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
    ylabel('pha [deg]'); xlabel('freq [rad/s]');
    legend('dynamics without delay','pure delay','dynamics including delay','Location','Best');
    
    % show the real and imaginary components using logarithmic frequency axes %
    figure(3); clf; zoom on;
    subplot(2,1,1); semilogx(w,real(Htest),'k', w,real(Hfull),'b'); grid on; hold on;
    tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
    tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
    ylabel('real [m/N]'); title('Real & Imaginary Components');
    subplot(2,1,2); semilogx(w,imag(Htest),'k', w,imag(Hfull),'b'); grid on; hold on;
    tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
    ylabel('imag [m/N]'); xlabel('freq [rad/s]');
    legend('dynamics without delay','dynamics including delay','Location','Best');
    
    % show the real-imag components using a Nyquist plot %
    figure(4); clf; zoom on;
    plot(real(Htest),imag(Htest),'k', real(Hfull),imag(Hfull),'b'); grid on; hold on;
    xlabel('real [m/N]'); ylabel('imag [m/N]'); title('Real & Imaginary Components');

end