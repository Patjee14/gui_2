% m function to remove an assumed delay effect in an FRF

function [FRF_removed] = remove_delay(w,FRF,Tdelay)

FRF_delay = cos(w*Tdelay) - j*sin(w*Tdelay);
FRF_removed = FRF ./ FRF_delay;