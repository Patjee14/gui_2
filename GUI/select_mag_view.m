Hfull = evalin('base','Hfull');
f = evalin('base','f');   

% show the bode plot (using linear frequency and magnitude axes) %
figure('Name','Choose linear maximum magnitude','pos',[100 100 870 375]); clf; zoom on;
plot(f,abs(Hfull),'b'); grid on; hold on;
ylabel('mag [-]'); title('Bode Plot (linear freq and mag axes)');
xlabel('freq [Hz]');
legend('First select the maximum magnitude','Location','southoutside'); 
    
[Xmag,Ymag] = ginput(1);
 Ymag = round(Ymag);
 mag_view_max = abs(Ymag);
 close;
 
 assignin('base','mag_view_max',mag_view_max);
 