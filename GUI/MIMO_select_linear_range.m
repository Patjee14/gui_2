Hfull = evalin('base','Hfull');
f = evalin('base','f');   
Nm = evalin('base','Nm');
Nn = evalin('base','Nn');

% show the bode plot (using linear frequency and magnitude axes) %
k = 1;
figure('Name','Choose linear frequency range','pos',[100 50 900 700]); clf; zoom on;
for i = 1:Nm
    for j = 1:Nn
subplot(Nm,Nn,k);
plot(f,abs(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
ylabel('mag [-]'); 
xlabel('freq [Hz]');
k = k + 1;
    end
end
legend('First select the minimum frequency and afterwards select the maximum frequency','Location','southoutside');
suptitle('Bode Plot (linear freq and mag axes)');

[Xlin,Ylin] = ginput(2);
 Xlin = round(Xlin);
 fview_min = Xlin(1);
 fview_max = Xlin(2);
 close;

kmin = ceil(interp1(f,(1:length(f))',fview_min));
kmax = floor(interp1(f,(1:length(f))',fview_max));
view_data_range = kmin:kmax;

% Assign all the values you need to workspace for the appdesigner
assignin('base','kmin',kmin);
assignin('base','kmax',kmax);
assignin('base','view_data_range',view_data_range);
assignin('base','fview_min',fview_min);
assignin('base','fview_max',fview_max);
