% load data file %
input_fname = evalin('base','input_fname');
tmp = load(input_fname);

%% To extract the transfer functions from the measurements structure.
f = tmp(:,1);
N = length(f);
y1 = tmp(:,2);
y2 = tmp(:,3);

w = 2*pi*f;
jw = j*2*pi*f;
r2d = 180/pi;

% Accelerometer Acceleration
Acc_R = y1; %%Real
Acc_I = y2; %%Imaginary
Acc_M=abs(Acc_R+sqrt(-1)*Acc_I);             %%Magnitude
Acc_P=unwrap(angle(Acc_R+sqrt(-1)*Acc_I));   %%Phase

G_Acc_Measurement = Acc_R + 1i*Acc_I;

% choose the a1vr transfer function to fit %
Hfull = G_Acc_Measurement;

% Assign all the values you need to workspace for the appdesigner
assignin('base','N',N);
assignin('base','f',f);
assignin('base','Hfull',Hfull);