Hfull = evalin('base','Hfull');
f = evalin('base','f');   

% show the bode plot (using linear frequency and magnitude axes) %
figure('Name','Choose linear frequency range','pos',[100 100 870 375]); clf; zoom on;
plot(f,abs(Hfull),'b'); grid on; hold on;
ylabel('mag [-]'); title('Bode Plot (linear freq and mag axes)');
xlabel('freq [Hz]');
legend('First select the minimum frequency and afterwards select the maximum frequency','Location','southoutside'); 
    
[Xlin,Ylin] = ginput(2);
 Xlin = round(Xlin);
 fview_min = Xlin(1);
 fview_max = Xlin(2);
 close;

kmin = ceil(interp1(f,(1:length(f))',fview_min));
kmax = floor(interp1(f,(1:length(f))',fview_max));
view_data_range = kmin:kmax;

% Assign all the values you need to workspace for the appdesigner
assignin('base','kmin',kmin);
assignin('base','kmax',kmax);
assignin('base','view_data_range',view_data_range);
assignin('base','fview_min',fview_min);
assignin('base','fview_max',fview_max);

