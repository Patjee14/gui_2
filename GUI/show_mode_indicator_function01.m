% Load all the data from workspace to use appdesigner
Hfull = evalin('base','Hfull');
fview_min = evalin('base','fview_min');
fview_max = evalin('base','fview_max');
view_data_range = evalin('base','view_data_range');
check_Nmodes = evalin('base','check_Nmodes');

%% compute mode indicator function 
MIF = Hfull .* conj(Hfull);
MIF_max = max(MIF(view_data_range));                                        % max and min values within vieweing range
MIF_norm = (1/MIF_max)*MIF;

    figure(15); clf; zoom on;
    plot(f,MIF_norm,'k'); grid on; hold on;
    title('Normalized Mode Indicator Function'); ylabel('MIF [%]'); xlabel('freq [Hz]');
    axis([fview_min , fview_max , 0 1]);

if check_Nmodes == 1  
Nmode = evalin('base','Nmode');
[X,Y] = ginput(2*Nmode);
X_round = round(2*X)/2;
else 
[X,Y] = ginput;
X_round = round(2*X)/2;   
end

for n = 1:(length(X_round)/2)
    fmin(n) = X_round(1+2*(n-1));
    fmax(n) = X_round(2+2*(n-1));
end
        
assignin('base','fmin',fmin);
assignin('base','fmax',fmax);
assignin('base','X_round',X_round);

close;


