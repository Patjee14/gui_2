% Load all the data from workspace to use appdesigner
counter = evalin('base','counter');
value_stable = evalin('base','value_stable');  
value_proper = evalin('base','value_proper');  

%% display models from smallest error to maximum error 
load(evalin('base','fitting_results'));
log_smallest_RMS_error = min(min(min(log10(fitting_RMS_error_array))));
log_largest_RMS_error  = max(max(max(log10(fitting_RMS_error_array))));
contour_array = linspace(floor(log_smallest_RMS_error),ceil(log_largest_RMS_error), 1000);

[sorted_RMS_error, sorting_index] = sort(fitting_RMS_error_log(:,1));

Ntest_cases = Ndelay_assumed*N_n*N_m;

k_sorting_index = counter;
    
    % retrieve stored case number and corressponding array parameters %
    case_number = sorting_index(k_sorting_index);
    fitting_RMS_error = fitting_RMS_error_log(case_number , 1);
    k_delay           = fitting_RMS_error_log(case_number , 2);
    k_n               = fitting_RMS_error_log(case_number , 3);
    k_m               = fitting_RMS_error_log(case_number , 4);
    
    % graphically show where the fit is, in terms of optimality %
    figure(30); clf; zoom on;
    contourf(n_array, m_array, log10(squeeze(fitting_RMS_error_array(k_delay,:,:)))'); hold on;
    caxis([log_smallest_RMS_error , log_largest_RMS_error]); view(0,90); colorbar;
    title(['Log10 of model fitting RMS error (Tdelay = ' num2str(Tdelay_assumed_array(k_delay)) ' [s])' ] );
    xlabel('Remaining dyn. denominator order: n [ ]');     ylabel('Remaining dyn. numerator order: m [ ]');
    grid on;

    plot(n_array(k_n), m_array(k_m) , 'ro'); plot(n_array(k_n), m_array(k_m) , 'yx');     
    text_handle = text(n_array(k_n), m_array(k_m), [num2str(log10(fitting_RMS_error))]);
    set(text_handle,'Color',[1 1 1]);
    
    % reconstruct fitting parameters and required graphs %
    Tdelay_assumed = Tdelay_assumed_array(k_delay);
    n = n_array (k_n); m = m_array (k_m);

    flag_show_figures = 0;
    remove_assumed_delay_from_frf01;                                            % remove delay and show TF
 
    fit_vibration_modes_compute_remainder_frf01;

    % fit_remaining_dynamics_note_error01;
    fit_remaining_dynamics_note_error03;

    readjust_coefficients_note_error03;
    
    assignin('base','n',n);
    assignin('base','m',m);
    assignin('base','Tdelay_assumed',Tdelay_assumed);
    
if show_final_figures
    
    figure(9); clf; zoom on;

    subplot(2,1,1); loglog(f,abs(Hdelay_removed),'k'); hold on;
    subplot(2,1,1); loglog(f,abs(total_modeled_modes_FRF),'b');
    subplot(2,1,1); loglog(f,abs(Hrem),'r');
    subplot(2,1,1); loglog(f, abs(Hremfit),'g--', f, abs(Hrem_plus_modes),'k.');
    legend( 'measurement with assumed delay removed','fitted modes',...
        'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');
    ylabel('mag [ ]');
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hdelay_removed)),'k'); hold on;
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(total_modeled_modes_FRF)),'b');
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hrem)),'r');
    subplot(2,1,2); semilogx(f, r2d*unwrap(angle(Hremfit)),'g--', f, r2d*unwrap(angle(Hrem_plus_modes)),'k.');
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    tmp = axis; axis([fview_min fview_max -1000 200]);
    
    figure(11); clf; zoom on;
    subplot(2,1,1); semilogx(f,real(Hdelay_removed),'k'); hold on;
    subplot(2,1,1); semilogx(f, real(total_modeled_modes_FRF), 'b');
    subplot(2,1,1); semilogx(f, real(Hrem), 'r');
    subplot(2,1,1); semilogx(f,real(Hremfit),'g--', f,real(Hrem_plus_modes),'k.');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    title('Test Data and Fitted TF'); ylabel('real [ ]');
    legend( 'measurement with assumed delay removed','fitted modes',...
            'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');

    subplot(2,1,2); semilogx(f,imag(Hdelay_removed),'k'); hold on;
    subplot(2,1,2); semilogx(f, imag(total_modeled_modes_FRF), 'b');
    subplot(2,1,2); semilogx(f, imag(Hrem), 'r');
    subplot(2,1,2); semilogx(f,imag(Hremfit),'g--', f,imag(Hrem_plus_modes),'k.');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    
    % compare overall fitted TF with originally 'measured' data %
    figure(1);
    subplot(2,1,1);
    loglog(f, abs(Hfull),'k',  f, abs(Hrem_plus_mode_with_assumed_delay), 'm--');
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    ylabel('mag [ ]'); title('Bode Plot (log freq and mag axes)'); grid on;
    subplot(2,1,2);
    semilogx(f, r2d*unwrap(angle(Hfull)),'k', f, r2d*unwrap(angle(Hrem_plus_mode_with_assumed_delay)), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
    tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]'); grid on;
    
    figure(2);
    subplot(2,1,1);
    plot(f, abs(Hfull),'k', f, abs(Hrem_plus_mode_with_assumed_delay), 'm--');
    tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
    ylabel('mag [ ]'); title('Bode Plot (linear freq and mag axes)'); grid on;
    subplot(2,1,2);
    plot(f, r2d*unwrap(angle(Hfull)), 'k', f, r2d*unwrap(angle(Hrem_plus_mode_with_assumed_delay)), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
    tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]'); grid on;
    
    figure(3);
    subplot(2,1,1);
    semilogx(f, real(Hfull), 'k', f, real(Hrem_plus_mode_with_assumed_delay), 'm--');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [ ]'); title('Real & Imaginary Components'); grid on;
    subplot(2,1,2);
    semilogx(f, imag(Hfull), 'k', f, imag(Hrem_plus_mode_with_assumed_delay), 'm--');
    legend('meas. dynamics','fitted TF','Location','Best');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]'); grid on;

    figure(13); clf; zoom on;
    subplot(2,1,1);
    semilogx(f, real(fitting_error),'k', f(data_range),real(fitting_error(data_range)),'b');
    title('Fitting Error'); ylabel('Real [ ]'); legend('Complete Data','Selected Range');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]); grid on;
    subplot(2,1,2);
    semilogx(f, imag(fitting_error),'k', f(data_range),imag(fitting_error(data_range)),'b');
    ylabel('Imag [ ]'); xlabel('freq [Hz]');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]); grid on;
    
    figure(14); clf; zoom on;
    pzmap(Gremfit); title('P-Z Map of Identified Remainder Dynamics (excl. modes)');

end
    