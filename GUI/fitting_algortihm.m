% Load all the data from workspace to use appdesigner
Nmode = evalin('base','Nmode');
fmin = evalin('base','fmin');
fmax = evalin('base','fmax');
Ndelay_assumed = evalin('base','Ndelay_assumed');
N_n = evalin('base','N_n');
N_m = evalin('base','N_m');
n_array = evalin('base','n_array');
m_array = evalin('base','m_array');
Tdelay_assumed_array = evalin('base','Tdelay_assumed_array');
fmin_global_fit = evalin('base','fmin_global_fit');
fmax_global_fit = evalin('base','fmax_global_fit');
mag_log_view_max = evalin('base','mag_log_view_max');
mag_log_view_min = evalin('base','mag_log_view_min');
mag_view_max = evalin('base','mag_view_max');
show_final_figures = evalin('base','show_final_figures');

%% actual fitting and search sequence
%  stepping through the given array of possible delays and remainder
%  dynamics denominator and numerator orders

fitting_RMS_error_array = zeros(Ndelay_assumed,N_n,N_m);                    % storage for curve fitting RMS values evaluated in each case
fitting_RMS_error_log = zeros(Ndelay_assumed*N_n*N_m,4);

flag_show_figures = 0;

% define_plant_frf01;                                                       % define plant FRF
load_plant_frf01;                                                           % load plant FRF from measurement data

% test different delay values and transfer function polynomial orders
% note the success in curve fitting
log_index = 0;
for k_delay = 1:Ndelay_assumed                                             % counter for tested delay values
    
    Tdelay_assumed = Tdelay_assumed_array(k_delay);
    
    for k_n = 1:N_n                                                        % counter for tested demonimator order for fitting remaining dynamics (after fitting the modes)
        
        n = n_array (k_n);
        
        for k_m = 1:N_m                                                    % counter for tested numerator order for fitting remaining dynamics (after fitting the modes)
        
            m = m_array (k_m);
            
            remove_assumed_delay_from_frf01;                                            % remove delay and show TF

            fit_vibration_modes_compute_remainder_frf01;
            
            % fit_remaining_dynamics_note_error01;
            fit_remaining_dynamics_note_error03;
            readjust_coefficients_note_error03;

            % note the error obtained in this case %
            fitting_RMS_error_array(k_delay,k_n,k_m) = fitting_error_RMS_selected_freq_range;
            log_index = log_index + 1;
            fitting_RMS_error_log(log_index,:) = [fitting_error_RMS_selected_freq_range , k_delay, k_n, k_m];

            % echo the tested parameters %
            [Tdelay_assumed , n , m]
            
        end
        
    end
    
end

save_and_assign;

% compare actual and fitted transfer functions %
if show_final_figures
    
    figure(9); clf; zoom on;

    subplot(2,1,1); loglog(f,abs(Hdelay_removed),'k'); hold on;
    subplot(2,1,1); loglog(f,abs(total_modeled_modes_FRF),'b');
    subplot(2,1,1); loglog(f,abs(Hrem),'r');
    subplot(2,1,1); loglog(f, abs(Hremfit),'g--', f, abs(Hrem_plus_modes),'k.');
    legend( 'measurement with assumed delay removed','fitted modes',...
        'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');
    ylabel('mag [ ]');
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hdelay_removed)),'k'); hold on;
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(total_modeled_modes_FRF)),'b');
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hrem)),'r');
    subplot(2,1,2); semilogx(f, r2d*unwrap(angle(Hremfit)),'g--', f, r2d*unwrap(angle(Hrem_plus_modes)),'k.');
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    tmp = axis; axis([fview_min fview_max -1000 200]);
    
    figure(11); clf; zoom on;
    subplot(2,1,1); semilogx(f,real(Hdelay_removed),'k'); hold on;
    subplot(2,1,1); semilogx(f, real(total_modeled_modes_FRF), 'b');
    subplot(2,1,1); semilogx(f, real(Hrem), 'r');
    subplot(2,1,1); semilogx(f,real(Hremfit),'g--', f,real(Hrem_plus_modes),'k.');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    title('Test Data and Fitted TF'); ylabel('real [ ]');
    legend( 'measurement with assumed delay removed','fitted modes',...
            'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');

    subplot(2,1,2); semilogx(f,imag(Hdelay_removed),'k'); hold on;
    subplot(2,1,2); semilogx(f, imag(total_modeled_modes_FRF), 'b');
    subplot(2,1,2); semilogx(f, imag(Hrem), 'r');
    subplot(2,1,2); semilogx(f,imag(Hremfit),'g--', f,imag(Hrem_plus_modes),'k.');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    
    % compare overall fitted TF with originally 'measured' data %
    figure(1);
    subplot(2,1,1);
    loglog(f, abs(Hfull),'k',  f, abs(Hrem_plus_mode_with_assumed_delay), 'm--');
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    ylabel('mag [ ]'); title('Bode Plot (log freq and mag axes)'); grid on;
    subplot(2,1,2);
    semilogx(f, r2d*unwrap(angle(Hfull)),'k', f, r2d*unwrap(angle(Hrem_plus_mode_with_assumed_delay)), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
    tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]'); grid on;
    
    figure(2);
    subplot(2,1,1);
    plot(f, abs(Hfull),'k', f, abs(Hrem_plus_mode_with_assumed_delay), 'm--');
    tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
    ylabel('mag [ ]'); title('Bode Plot (linear freq and mag axes)'); grid on;
    subplot(2,1,2);
    plot(f, r2d*unwrap(angle(Hfull)), 'k', f, r2d*unwrap(angle(Hrem_plus_mode_with_assumed_delay)), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
    tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]'); grid on;
    
    figure(3);
    subplot(2,1,1);
    semilogx(f, real(Hfull), 'k', f, real(Hrem_plus_mode_with_assumed_delay), 'm--');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [ ]'); title('Real & Imaginary Components'); grid on;
    subplot(2,1,2);
    semilogx(f, imag(Hfull), 'k', f, imag(Hrem_plus_mode_with_assumed_delay), 'm--');
    legend('meas. dynamics','fitted TF','Location','Best');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]'); grid on;

    figure(13); clf; zoom on;
    subplot(2,1,1);
    semilogx(f, real(fitting_error),'k', f(data_range),real(fitting_error(data_range)),'b');
    title('Fitting Error'); ylabel('Real [ ]'); legend('Complete Data','Selected Range');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]); grid on;
    subplot(2,1,2);
    semilogx(f, imag(fitting_error),'k', f(data_range),imag(fitting_error(data_range)),'b');
    ylabel('Imag [ ]'); xlabel('freq [Hz]');
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]); grid on;
    
    figure(14); clf; zoom on;
    pzmap(Gremfit); title('P-Z Map of Identified Remainder Dynamics (excl. modes)');

end

