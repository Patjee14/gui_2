% Load all the data from workspace to use appdesigner
counter = evalin('base','counter');

%% display models from smallest error to maximum error 
load(evalin('base','fitting_results'));
log_smallest_RMS_error = min(min(min(log10(fitting_RMS_error_array))));
log_largest_RMS_error  = max(max(max(log10(fitting_RMS_error_array))));
contour_array = linspace(floor(log_smallest_RMS_error),ceil(log_largest_RMS_error), 1000);

[sorted_RMS_error, sorting_index] = sort(fitting_RMS_error_log(:,1));

Ntest_cases = Ndelay_assumed*N_n*N_m;

k_sorting_index = counter;

    % retrieve stored case number and corressponding array parameters %
    case_number = sorting_index(k_sorting_index);
    fitting_RMS_error = fitting_RMS_error_log(case_number , 1);
    k_delay           = fitting_RMS_error_log(case_number , 2);
    k_n               = fitting_RMS_error_log(case_number , 3);
    k_m               = fitting_RMS_error_log(case_number , 4);
    
    % graphically show where the fit is, in terms of optimality %
    figure(30); clf; zoom on;
    contourf(n_array, m_array, log10(squeeze(fitting_RMS_error_array(k_delay,:,:)))'); hold on;
    caxis([log_smallest_RMS_error , log_largest_RMS_error]); view(0,90); colorbar;
    title(['Log10 of model fitting RMS error (Tdelay = ' num2str(Tdelay_assumed_array(k_delay)) ' [s])' ] );
    xlabel('Remaining dyn. denominator order: n [ ]');     ylabel('Remaining dyn. numerator order: m [ ]');
    grid on;

    plot(n_array(k_n), m_array(k_m) , 'ro'); plot(n_array(k_n), m_array(k_m) , 'yx');     
    text_handle = text(n_array(k_n), m_array(k_m), [num2str(log10(fitting_RMS_error))]);
    set(text_handle,'Color',[1 1 1]);
    
    % reconstruct fitting parameters and required graphs %
    Tdelay_assumed = Tdelay_assumed_array(k_delay);
    n = n_array (k_n); m = m_array (k_m);

    flag_show_figures = 0;
    remove_assumed_delay_from_frf_MIMO;                                            % remove delay and show TF
   
    fit_vibration_modes_compute_remainder_frf_MIMO;

    % fit_remaining_dynamics_note_error01;
    fit_remaining_dynamics_note_error_MIMO;
    
    readjust_coefficients_note_error_MIMO;
    
    assignin('base','n',n);
    assignin('base','m',m);
    assignin('base','Tdelay_assumed',Tdelay_assumed);
    
% compare actual and fitted transfer functions %
if  show_final_figures
   k = 1;
   for  i = 1:Nm
       for j = 1:Nn
    figure(9); zoom on;
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    title(['G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(total_modeled_modes_FRF(i,j,:))),'b');
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hrem(i,j,:))),'r');
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hremfit(i,j,:))),'g--', f, abs(squeeze(Hrem_plus_modes(i,j,:))),'k.');
    legend( 'measurement with assumed delay removed','fitted modes',...
        'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');
    ylabel('mag [ ]');
    axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hdelay_removed(i,j,:)))),'k'); hold on;
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(total_modeled_modes_FRF(i,j,:)))),'b');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hrem(i,j,:)))),'r');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hremfit(i,j,:)))),'g--', f, r2d*unwrap(angle(squeeze(Hrem_plus_modes(i,j,:)))),'k.');
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    axis([fview_min fview_max -1000 200]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(11);  zoom on;
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(total_modeled_modes_FRF(i,j,:))), 'b');
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hrem(i,j,:))), 'r');
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hremfit(i,j,:))),'g--', f,real(squeeze(Hrem_plus_modes(i,j,:))),'k.');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    title(['Test Data and Fitted TF G' num2str(i) num2str(j)]); ylabel('real [ ]');
    legend( 'measurement with assumed delay removed','fitted modes',...
            'remainder term','fit of remainder term','reconstructed dynamics (excluding delay)');

    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hdelay_removed(i,j,:))),'k'); hold on;
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(total_modeled_modes_FRF(i,j,:))), 'b');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hrem(i,j,:))), 'r');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hremfit(i,j,:))),'g--', f,imag(squeeze(Hrem_plus_modes(i,j,:))),'k.');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    % compare overall fitted TF with originally 'measured' data %
    figure(1);
    subplot(2*Nm,Nn,k);
    loglog(f, abs(squeeze(Hfull(i,j,:))),'k',  f, abs(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
    axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    ylabel('mag [ ]'); title(['Bode Plot (log freq and mag axes) G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, r2d*unwrap(angle(squeeze(Hfull(i,j,:)))),'k', f, r2d*unwrap(angle(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:)))), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
    axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(2);
    subplot(2*Nm,Nn,k);
    plot(f, abs(squeeze(Hfull(i,j,:))),'k', f, abs(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
    axis([fview_min fview_max 0 mag_view_max]);
    ylabel('mag [ ]'); title(['Bode Plot (linear freq and mag axes) G' num2str(i) num2str(j)]);
    subplot(2*Nm,Nn,k+Nn);
    plot(f, r2d*unwrap(angle(squeeze(Hfull(i,j,:)))), 'k', f, r2d*unwrap(angle(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:)))), 'm--');
    legend('meas. dynamics','fitted TF (incl. delay)','Location','Best');
	axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(3);
    subplot(2*Nm,Nn,k);
    semilogx(f, real(squeeze(Hfull(i,j,:))), 'k', f, real(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [ ]'); title(['Real & Imaginary Components G' num2str(i) num2str(j)]); 
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, imag(squeeze(Hfull(i,j,:))), 'k', f, imag(squeeze(Hrem_plus_mode_with_assumed_delay(i,j,:))), 'm--');
    legend('meas. dynamics','fitted TF','Location','Best');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [ ]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    figure(13); zoom on;
    subplot(2*Nm,Nn,k);
    semilogx(f, real(squeeze(fitting_error(i,j,:))),'k', f(data_range),real(squeeze(fitting_error(i,j,data_range))),'b');
    title(['Fitting Error G' num2str(i) num2str(j)]); ylabel('Real [ ]'); legend('Complete Data','Selected Range');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    subplot(2*Nm,Nn,k+Nn);
    semilogx(f, imag(squeeze(fitting_error(i,j,:))),'k', f(data_range),imag(squeeze(fitting_error(i,j,data_range))),'b');
    ylabel('Imag [ ]'); xlabel('freq [Hz]');
    axis([fview_min fview_max -mag_view_max mag_view_max]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
       k = k + 1; 
       end
       k = 2*Nn+1+(i-1)*Nn*2; 
    end
end

if show_final_figures
   k = 1;
   for  i = 1:Nm
       for j = 1:Nn
    figure(14); zoom on;
    subplot(Nm,Nn,k);
    pzmap(squeeze(Gremfit(i,j,:))); 
    k = k + 1;
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
        end
   end
   suptitle('P-Z Map of Identified Remainder Dynamics (excl. modes)');
end