function [X] = evaluate_objective_function_MIMO(theta_normalized, other_parameters)
% ======================================================================= %
% m function to evaluate the RMS of model fitting error                   %
% ======================================================================= %

persistent Phi alpha_k beta_k gamma_k delta_k
persistent wn zeta p Tdelay optim_var_vector g_k h_k t_k u_k w_power theta
persistent Y 



% auxiliary parameters %
optim_var_range = other_parameters.optim_var_range;
wn_range        = other_parameters.wn_range;
zeta_range      = other_parameters.zeta_range;
p_range         = other_parameters.p_range;
Tdelay_range    = other_parameters.Tdelay_range;
Nc              = other_parameters.Nc;
Nr              = other_parameters.Nr;
Nd              = other_parameters.Nd;
Nrange          = other_parameters.Nrange;
data_range      = other_parameters.data_range;
w1              = other_parameters.w1;
w2              = other_parameters.w2;
Hdelay_removed  = other_parameters.Hdelay_removed;
Hfull           = other_parameters.Hfull;
Nm              = other_parameters.Nm;
Nn              = other_parameters.Nn;

% to speed up the calculations, pre-allocate memory for the arrays %
if isempty(Phi)
    Phi = zeros(2*Nrange,2*Nc + Nr + Nd);                                       % pre-allocate this array (and possibly make it global or static)
    alpha_k = zeros(Nc,1); beta_k = zeros(Nc,1); gamma_k = zeros(Nr,1); delta_k = zeros(Nd,1);
    wn = zeros(Nc,1);      zeta = zeros(Nc,1);   p = zeros(Nr,1);       Tdelay = 0;
    theta = zeros(2*Nc + Nr + 1);
    g_k = zeros(Nrange,1);
    h_k = zeros(Nrange,1);
    t_k = zeros(Nrange,1);
    u_k = zeros(Nrange,1);
    w_power = zeros(Nrange,1);
    theta = zeros(2*Nc + Nr + Nd);
    Y = zeros(2*Nrange,1);
end

% de-normalize optimization variable vector %
optim_var_vector = optim_var_range.*theta_normalized;

% wn, zeta, and p (real pole) values in the new ordering
wn     = optim_var_vector(wn_range);                                          % the wn and zeta values (from both the modes and 'remainder' dynamics)
zeta   = optim_var_vector(zeta_range);
p      = optim_var_vector(p_range);                                           % real pole values
Tdelay = optim_var_vector(Tdelay_range);

% ======================================================================= %
% construct portion of the regressor matrix related to complex pole pairs %
% ======================================================================= %
for k = 1:Nc
    % for q = 1:Nm*Nn

    % FRF contribution of mode k (assuming unit mass) %
    g_k = (wn(k)*wn(k) - w2) ./ ((wn(k).*wn(k) - w2).*(wn(k).*wn(k) - w2) + (2*zeta(k)*wn(k)*w1).*(2*zeta(k)*wn(k)*w1));
    h_k = (-2*zeta(k)*wn(k)*w1) ./ ((wn(k).*wn(k) - w2).*(wn(k).*wn(k) - w2) + (2*zeta(k)*wn(k)*w1).*(2*zeta(k)*wn(k)*w1));
    
    % update portions of regressor matrix related to the real and imaginary
    % components of the dynamic response
%     Phi((2*Nrange)*(q-1)+1 :(2*Nrange)*(q-1)+Nrange , 2*(k-1) + (1:2)) = [g_k , -w1.*h_k];
%     Phi((2*Nrange)*(q-1)+1+Nrange: (2*Nrange)*(q) , 2*(k-1) + (1:2)) = [h_k ,  w1.*g_k];
    Phi(         1:    Nrange , 2*(k-1) + (1:2)) = [g_k , -w1.*h_k];
    Phi((Nrange+1):(2*Nrange) , 2*(k-1) + (1:2)) = [h_k ,  w1.*g_k];
%   end
end

% ======================================================================= %
% construct portion of the regressor matrix related to real poles         %
% ======================================================================= %
for k = 1:Nr
%     for q = 1:Nm*Nn
    % FRF contribution of pole k %
    t_k = p(k) ./ ( p(k)*p(k) + w2);
    u_k = -w1  ./ ( p(k)*p(k) + w2);
    
    % update portions of regressor matrix related to the real and imaginary
    % components of the dynamic response
%     Phi((2*Nrange)*(q-1)+1 :(2*Nrange)*(q-1)+Nrange , 2*Nc + k ) = [t_k];
%     Phi((2*Nrange)*(q-1)+1+Nrange: (2*Nrange)*(q) , 2*Nc + k ) = [u_k];
    Phi(         1:    Nrange , 2*Nc + k ) = [t_k];
    Phi((Nrange+1):(2*Nrange) , 2*Nc + k ) = [u_k];

%     end
end

% ======================================================================= %
% construct portion of the regressor matrix related to direct terms       %
% ======================================================================= %
w_power = ones(Nrange,1);
for k = 1:Nd

    if mod(k,4) == 1
        Phi(         1:    Nrange , 2*Nc + Nr + k ) = w_power;
        Phi( (Nrange+1):(2*Nrange), 2*Nc + Nr + k ) = zeros(Nrange,1);
    end
    
    if mod(k,4) == 2
        Phi(         1:    Nrange , 2*Nc + Nr + k ) = zeros(Nrange,1);
        Phi( (Nrange+1):(2*Nrange), 2*Nc + Nr + k ) = w_power;
    end
    
    if mod(k,4) == 3
        Phi(         1:    Nrange , 2*Nc + Nr + k ) = -w_power;
        Phi( (Nrange+1):(2*Nrange), 2*Nc + Nr + k ) =  zeros(Nrange,1);
    end
    
    if mod(k,4) == 0
        Phi(         1:    Nrange , 2*Nc + Nr + k ) = zeros(Nrange,1);
        Phi( (Nrange+1):(2*Nrange), 2*Nc + Nr + k ) = -w_power;
    end
    
    w_power = w_power .* w1;

end

for  i = 1:Nm
    for j = 1:Nn 
% output vector %
Y = [real(squeeze(Hdelay_removed(i,j,data_range))) ; imag(squeeze(Hdelay_removed(i,j,data_range))) ]; % real and imaginary components of 'measured' FRF after assumed delay removal

% Basic LS - no weighting %
theta = pinv(Phi)*Y;

% weighted LS %
% W = eye(2*Nrange);                                                          % do not use any weights
% W = diag([1./abs(Hdelay_removed(data_range)) ; 1./abs(Hdelay_removed(data_range))]);                                  % use reciprocal of measured TF amplitude as weighting function
% W = diag([1./(abs(Hdelay_removed(data_range)).*abs(Hdelay_removed(data_range))) ; 1./(Hdelay_removed(Hrem(data_range)).*abs(Hdelay_removed(data_range)))]);        % use square of reciprocal of measured TF amplitude as weighting function
% theta = inv(Phi'*W*Phi)*Phi'*W*Y;                                           % solve parameters

% extract identified parameters into new TF array %
% for k = 1:Nc,
%     alpha_k(k) = theta(2*(k-1) + 1);
%     beta_k(k) =  theta(2*(k-1) + 2);
% end
% 
% for k = 1:Nr,
%     gamma_k(k) = theta(2*Nc + k);
% end
% 
% for k = 1:Nd,
%     delta_k(k) = theta(2*Nc + Nr + k);
% end

% estimate predicted response %
Yest(i,j,:) = Phi*theta;

% delay effect %
Hdelay_assumed_data_range = cos(w1*Tdelay) - 1i*sin(w1*Tdelay);
Yest_delay(i,j,:) = Hdelay_assumed_data_range .* (squeeze(Yest(i,j,1:Nrange)) + 1i*squeeze(Yest(i,j,(Nrange+1):(2*Nrange))));
 
% estimate RMS error %
E(i,j,:) = squeeze(Hfull(i,j,data_range)) - squeeze(Yest_delay(i,j,:));
% E = Phi*theta - Y;
    
% return the objective value %
J(i,j,:) = sqrt(squeeze(E(i,j,:))'*squeeze(E(i,j,:))/(Nrange));
    end
end

X = norm(squeeze(J));