% Load all the data from workspace to use appdesigner

%% apply global improvement for the mode residuals (i.e., mode participation factors)
%  and numerator coefficients of the fitted remainder dynamics (i.e.,residual)
%  transfer function

% ======================================================================= %
% construct portion of the regressor matrix related to vibration modes%   %
% ======================================================================= %
w2 = w1.*w1;                                                                % w1 was already definde (above) as the global fitting frequency array
Phi = [];

for kmode = 1:Nmode
    
    % FRF contribution of mode k (assuming unit mass) %
    g_k = (wn(kmode)*wn(kmode) - w2) ./ ((wn(kmode).*wn(kmode) - w2).*(wn(kmode).*wn(kmode) - w2) + (2*zeta(kmode)*wn(kmode)*w1).*(2*zeta(kmode)*wn(kmode)*w1));
    h_k = (-2*zeta(kmode)*wn(kmode)*w1) ./ ((wn(kmode).*wn(kmode) - w2).*(wn(kmode).*wn(kmode) - w2) + (2*zeta(kmode)*wn(kmode)*w1).*(2*zeta(kmode)*wn(kmode)*w1));
    
    % update portions of regressor matrix related to the real and imaginary
    % components of the dynamic response
    Phi_real = [g_k , -w1.*h_k];
    Phi_imag = [h_k ,  w1.*g_k];
    Phi = [Phi , [ Phi_real ; Phi_imag]];

end

% ======================================================================= %
% construct portion of the regressor matrix related to the remainder
% dynamics
% ======================================================================= %
% FRF contribution of identified denominator (p & q)%
t = zeros(Nrange,1); u = zeros(Nrange,1);                                   % intermediate arrays to construct FRF of denominator portion
if n>0
    t = t + a_recip_fit(1);
end
% else
%     t = t + 1;


w_power = ones(Nrange,1);

for k = 1:(n-1)
    
    w_power = w_power .* w1;
    
    if mod(k,4) == 1
        u = u + w_power * a_recip_fit(k+1);
    end
    
    if mod(k,4) == 2
        t = t - w_power * a_recip_fit(k+1);
    end
    
    if mod(k,4) == 3
        u = u - w_power * a_recip_fit(k+1);
    end
    
    if mod(k,4) == 0
        t = t + w_power * a_recip_fit(k+1);
    end
end

if n>0
    k = n;
    w_power = w_power .* w1;
    if mod(k,4) == 1, u = u + w_power ; end
    if mod(k,4) == 2, t = t - w_power ; end
    if mod(k,4) == 3, u = u - w_power ; end
    if mod(k,4) == 0, t = t + w_power ; end
end


if n>0
p =  t ./ (t.*t + u.*u);
q = -u ./ (t.*t + u.*u);
else
    p = ones(Nrange,1);
    q = zeros(Nrange,1);
end

% % just verification %
% Rfit = tf([1],[a_fit]);
% Rf = squeeze(freqresp(Rfit,w1));
% figure(20); clf; zoom on;
% subplot(2,1,1); plot(w1,p,'b', w1,real(Rf),'r--');
% subplot(2,1,2); plot(w1,q,'b', w1,imag(Rf),'r--');
% 
% pause;

Phi_real = [p];
Phi_imag = [q];
Phi = [Phi , [Phi_real ; Phi_imag] ];

w_power = ones(Nrange,1);
for k = 1:m
    
    w_power = w_power .* w1;

    if mod(k,4) == 1
        Phi_real = -w_power .* q;
        Phi_imag =  w_power .* p;
        Phi = [Phi , [Phi_real ; Phi_imag] ];
    end
    
    if mod(k,4) == 2
        Phi_real = -w_power .* p;
        Phi_imag = -w_power .* q;
        Phi = [Phi , [Phi_real ; Phi_imag] ];
    end
    
    if mod(k,4) == 3
        Phi_real =  w_power .* q;
        Phi_imag = -w_power .* p;
        Phi = [Phi , [Phi_real ; Phi_imag] ];
    end
    
    if mod(k,4) == 0
        Phi_real =  w_power .* p;
        Phi_imag =  w_power .* q;
        Phi = [Phi , [Phi_real ; Phi_imag] ];
    end
    
end

% output vector %
Y = [real(Hdelay_removed(data_range)) ; imag(Hdelay_removed(data_range)) ]; % real and imaginary components of 'measured' FRF after assumed delay removal

% Basic LS - no weighting %
theta = pinv(Phi)*Y;

% weighted LS %
% W = eye(2*Nrange);                                                          % do not use any weights
% W = diag([1./abs(Hdelay_removed(data_range)) ; 1./abs(Hdelay_removed(data_range))]);                                  % use reciprocal of measured TF amplitude as weighting function
% W = diag([1./(abs(Hdelay_removed(data_range)).*abs(Hdelay_removed(data_range))) ; 1./(Hdelay_removed(Hrem(data_range)).*abs(Hdelay_removed(data_range)))]);        % use square of reciprocal of measured TF amplitude as weighting function
% theta = inv(Phi'*W*Phi)*Phi'*W*Y;                                           % solve parameters

% extract identified parameters into new TF array %
for kmode = 1:Nmode
    alpha_k(kmode) = theta(2*(kmode-1) + 1);
    beta_k(kmode) =  theta(2*(kmode-1) + 2);
end

for k = 1:(m+1)
    b_recip_fit(k) = theta(2*Nmode + k);
end

b_fit = (flipud(b_recip_fit))';                                             % numerator as b0*s^m + b1*s^{m-1} + ... + bm
Gremfit = tf([b_fit],[a_fit]);

% ======================================================================= %
% reconstruct new model                                                   %
% ======================================================================= %

% contribution of 'newly tuned' modes %
mode_FRF = zeros(length(w),Nmode);
total_modeled_modes_FRF = zeros(length(w),1);

for kmode = 1:Nmode
    
    g2 = (wn(kmode)*wn(kmode) - w.*w) ./ ((wn(kmode).*wn(kmode) - w.*w).*(wn(kmode).*wn(kmode) - w.*w) + (2*zeta(kmode)*wn(kmode)*w).*(2*zeta(kmode)*wn(kmode)*w));
    h2 = (-2*zeta(kmode)*wn(kmode)*w) ./ ((wn(kmode).*wn(kmode) - w.*w).*(wn(kmode).*wn(kmode) - w.*w) + (2*zeta(kmode)*wn(kmode)*w).*(2*zeta(kmode)*wn(kmode)*w));
       
    mode_FRF(:,kmode) = (alpha_k(kmode) + j*w*beta_k(kmode)).*(g2 + j*h2);
    
    % add the contribution of the current mode to the 'modeled' FRF %
    total_modeled_modes_FRF = total_modeled_modes_FRF + mode_FRF(:,kmode);

    if flag_show_figures
        
        figure(5);
        subplot(2,1,1); loglog(f,abs(mode_FRF(:,kmode)),'g:');
        ylabel('mag [ ]'); title('Bode Plot (log freq and mag axes)');
        tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
        subplot(2,1,2); semilogx(f,r2d*unwrap(angle(mode_FRF(:,kmode))),'g:');
        ylabel('pha [deg]'); xlabel('freq [Hz]');
        tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(6);
        subplot(2,1,1); plot(f,abs(mode_FRF(:,kmode)),'g:');
        ylabel('mag [ ]'); title('Bode Plot (linear freq and mag axes)');
        tmp = axis; axis([fview_min fview_max 0 mag_view_max]);
        subplot(2,1,2); plot(f,r2d*unwrap(angle(mode_FRF(:,kmode))),'g:');
        ylabel('pha [deg]'); xlabel('freq [Hz]');
        tmp = axis; axis([fview_min fview_max -1000 200]);
        
        figure(7);
        subplot(2,1,1); semilogx(f, real(mode_FRF(:,kmode)), 'g:');
        ylabel('real [ ]'); title('Real & Imaginary Components');
        tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
        subplot(2,1,2); semilogx(f, imag(mode_FRF(:,kmode)), 'g:');
        ylabel('imag [ ]'); xlabel('freq [Hz]');
        tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
        
        figure(8);
        plot(real(mode_FRF(:,kmode)), imag(mode_FRF(:,kmode)), 'g:');
        xlabel('real [ ]'); ylabel('imag [ ]'); title('Real & Imaginary Components (selected viewing data range)');
        axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);

    end

    % pause;
end


% if flag_show_figures,
%     
%     figure(9); clf; zoom on;
%     subplot(2,1,1); loglog(w,abs(Hdelay_removed),'k',w,abs(total_modeled_modes_FRF),'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     subplot(2,1,2); semilogx(w,r2d*unwrap(angle(Hdelay_removed)),'k', w,r2d*unwrap(angle(total_modeled_modes_FRF)),'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     
%     figure(10); clf; zoom on;
%     subplot(2,1,1); plot(w,abs(Hdelay_removed),'k',w,abs(total_modeled_modes_FRF),'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     subplot(2,1,2); plot(w,r2d*unwrap(angle(Hdelay_removed)),'k', w,r2d*unwrap(angle(total_modeled_modes_FRF)),'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     
%     figure(11); clf; zoom on;
%     subplot(2,1,1); semilogx(w, real(Hdelay_removed), 'k', w, real(total_modeled_modes_FRF), 'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     subplot(2,1,2); semilogx(w, imag(Hdelay_removed), 'k', w, imag(total_modeled_modes_FRF), 'b.');
%     tmp = axis; axis([fview_min fview_max tmp(3) tmp(4)]);
%     
%     figure(12); clf; zoom on;
%     plot(real(Hdelay_removed), imag(Hdelay_removed), 'k', real(total_modeled_modes_FRF), imag(total_modeled_modes_FRF), 'b.');
% 
% end

% construct the fitted transfer function %
Hremfit = squeeze(freqresp(Gremfit,w));

% combine fitted remainder TF with fitted contributions from the modes %
Hrem_plus_modes = total_modeled_modes_FRF + Hremfit;                        % reconstructed t.f. with summing contributions due to vibration modes and other dynamics (as partial fractions)
Hrem_plus_mode_with_assumed_delay = Hrem_plus_modes .* Hdelay_assumed;      % also, the effect of the assumed delay is incorprated as well

% compute fitting error, and its RMS value in selected frequency range %]
fitting_error = Hfull - Hrem_plus_mode_with_assumed_delay;
fitting_error_RMS_selected_freq_range = sqrt( fitting_error(data_range)' * fitting_error(data_range) / Nrange );
