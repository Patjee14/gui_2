% m file to remove assumed delay from measurement FRF for rational 
% polynomial transfer function fitting

Hdelay_assumed = cos(w*Tdelay_assumed) - 1i*sin(w*Tdelay_assumed);
Hdelay_removed = (Hfull ./ Hdelay_assumed);

if flag_show_figures

    % show the delay removed bode plot %
    figure(5);
    subplot(2,1,1); loglog(f,abs(Hdelay_removed),'k'); grid on; hold on;
    tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    ylabel('mag [m/N]'); title('Bode Plot (log freq and mag axes)');
    legend('Dynamics with assumed delay removed','Location','Best');
    subplot(2,1,2); semilogx(f,r2d*unwrap(angle(Hdelay_removed)),'k'); grid on; hold on;
    tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    
    % show the bode plot (using linear frequency and magnitude axes) %
    figure(6);
    subplot(2,1,1); plot(f,abs(Hdelay_removed),'k'); grid on; hold on;
    tmp = axis; axis([fview_min fview_max tmp(3) mag_view_max]);
    ylabel('mag [m/N]'); title('Bode Plot (linear freq and mag axes)');
    legend('Dynamics with assumed delay removed','Location','Best');
    subplot(2,1,2); plot(f,r2d*unwrap(angle(Hdelay_removed)),'k'); grid on; hold on;
    tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    
    % show the real and imaginary components using logarithmic frequency axes %
    figure(7);
    subplot(2,1,1); semilogx(f,real(Hdelay_removed),'k'); grid on; hold on;
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [m/N]'); title('Real & Imaginary Components');
    legend('Dynamics with assumed delay removed','Location','Best');
    subplot(2,1,2); semilogx(f,imag(Hdelay_removed),'k'); grid on; hold on;
    tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [m/N]'); xlabel('freq [Hz]');
    
    % show the real-imag components using a Nyquist plot %
    k_view_min = ceil(interp1(f,(1:N)',fview_min));
    k_view_max = floor(interp1(f,(1:N)',fview_max));
    view_data_range = (k_view_min:k_view_max)';

    % show the real-imag components using a Nyquist plot %
    figure(8);
    plot(real(Hdelay_removed(view_data_range)), imag(Hdelay_removed(view_data_range)), 'k'); grid on; hold on;
    legend('Dynamics with assumed delay removed','Location','Best');
    xlabel('real [m/N]'); ylabel('imag [m/N]'); title('Real & Imaginary Components');
    axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);

end
