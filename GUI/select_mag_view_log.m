    Hfull = evalin('base','Hfull');
    f = evalin('base','f'); 
       
    figure('Name','Choose logarithmic magnitude range','pos',[100 100 870 375])
    clf; loglog(f,abs(Hfull),'b'); grid on; hold on;
    ylabel('mag [ ]'); title('Bode Plot (log freq and mag axes)');
    xlabel('freq [Hz]');
    legend('First select the minimum magnitude and afterwards select the maximum magnitude','Location','southoutside'); 
    
    [Xlin,Ylin] = ginput(2);
    Ylin = round(Ylin,3);
    mag_log_view_min = Ylin(1);
    mag_log_view_max = Ylin(2);
    close;
    
    assignin('base','mag_log_view_min',mag_log_view_min);
    assignin('base','mag_log_view_max',mag_log_view_max);
  
 