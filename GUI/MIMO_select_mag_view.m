Hfull = evalin('base','Hfull');
f = evalin('base','f');   
Nm = evalin('base','Nm');
Nn = evalin('base','Nn');

% show the bode plot (using linear frequency and magnitude axes) %
k = 1;
figure('Name','Choose linear maximum magnitude','pos',[100 50 900 700]); clf; zoom on;
for i = 1:Nm
    for j = 1:Nn
subplot(Nm,Nn,k);
plot(f,abs(squeeze(Hfull(i,j,:))),'b'); grid on; hold on;
ylabel('mag [-]'); 
xlabel('freq [Hz]');
k = k + 1;
    end
end
legend('First select the maximum magnitude','Location','southoutside');
suptitle('Bode Plot (linear freq and mag axes)');

    
[Xmag,Ymag] = ginput(1);
 Ymag = round(Ymag);
 mag_view_max = abs(Ymag);
 close;
 
 assignin('base','mag_view_max',mag_view_max);
