% Load all the data from workspace to use appdesigner
MIF_norm = evalin('base','MIF_norm');
check_Nmodes = evalin('base','check_Nmodes');
fview_min = evalin('base','fview_min');
fview_max = evalin('base','fview_max');
f = evalin('base','f');
Nn = evalin('base','Nn');
Nm = evalin('base','Nm');

k = 1;
figure('Name','Choose modes','pos',[100 50 900 700]); clf; zoom on;
    for  i = 1:Nm
        for j = 1:Nn
    subplot(Nm,Nn,k)
    plot(f,squeeze(MIF_norm(i,j,:)),'k'); grid on; hold on;
    title(['Normalized Mode Indicator Function G' num2str(i) num2str(j)]); ylabel('MIF [%]'); xlabel('freq [Hz]');
    axis([fview_min , fview_max , 0 1]);
    k = k + 1;
        end
    end   
legend('Choose modes from left to right and press enter after finishing','Location','southoutside');

if check_Nmodes == 1  
Nmode = evalin('base','Nmode');
[X,Y] = ginput(2*Nmode);
X_round = round(2*X)/2;
else 
[X,Y] = ginput;
X_round = round(2*X)/2;   
end

for n = 1:(length(X_round)/2)
    fmin(n) = X_round(1+2*(n-1));
    fmax(n) = X_round(2+2*(n-1));
end
        
assignin('base','fmin',fmin);
assignin('base','fmax',fmax);
assignin('base','X_round',X_round);

close;


