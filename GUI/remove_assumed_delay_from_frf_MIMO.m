% m file to remove assumed delay from measurement FRF for rational 
% polynomial transfer function fitting
k = 1;
for i = 1:Nm
    for j = 1:Nn
Hdelay_assumed(i,j,:) = cos(w*Tdelay_assumed) - 1i*sin(w*Tdelay_assumed);
Hdelay_removed(i,j,:) = (Hfull(i,j,:) ./ Hdelay_assumed(i,j,:));
 
    if flag_show_figures

    % show the delay removed bode plot %
    figure(5); zoom on;
    subplot(2*Nm,Nn,k); loglog(f,abs(squeeze(Hdelay_removed(i,j,:))),'k'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max mag_log_view_min mag_log_view_max]);
    ylabel('mag [m/N]'); title(['Bode Plot (log freq and mag axes) G' num2str(i) num2str(j)]);
    legend('Dynamics with assumed delay removed','Location','Best');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,r2d*unwrap(angle(squeeze(Hdelay_removed(i,j,:)))),'k'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
        
    % show the bode plot (using linear frequency and magnitude axes) %
    figure(6); zoom on;            
    subplot(2*Nm,Nn,k); plot(f,abs(squeeze(Hdelay_removed(i,j,:))),'k'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max tmp(3) mag_view_max]);
    ylabel('mag [m/N]'); title(['Bode Plot (linear freq and mag axes) G' num2str(i) num2str(j)]);
    legend('Dynamics with assumed delay removed G','Location','Best');
    subplot(2*Nm,Nn,k+Nn); plot(f,r2d*unwrap(angle(squeeze(Hdelay_removed(i,j,:)))),'k'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max -1000 200]);
    ylabel('pha [deg]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    % show the real and imaginary components using logarithmic frequency axes %
    figure(7); zoom on;       
    subplot(2*Nm,Nn,k); semilogx(f,real(squeeze(Hdelay_removed(i,j,:))),'k'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('real [m/N]'); title(['Real & Imaginary Components G' num2str(i) num2str(j)]);
    legend('Dynamics with assumed delay removed','Location','Best');
    subplot(2*Nm,Nn,k+Nn); semilogx(f,imag(squeeze(Hdelay_removed(i,j,:))),'k'); grid on; hold on;
%     tmp = axis; axis([fview_min fview_max -mag_view_max mag_view_max]);
    ylabel('imag [m/N]'); xlabel('freq [Hz]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
        end
    k = k + 1;
        end
    k = 2*Nn+1+(i-1)*Nn*2; 
end
    % show the real-imag components using a Nyquist plot %
    k_view_min = ceil(interp1(f,(1:N)',fview_min));
    k_view_max = floor(interp1(f,(1:N)',fview_max));
    view_data_range = (k_view_min:k_view_max)';

if flag_show_figures
    
    % show the real-imag components using a Nyquist plot %
    figure(8);clf; zoom on;
    k = 1;
    for  i = 1:Nm
        for j = 1:Nn
    subplot(Nm,Nn,k);  
    plot(real(squeeze(Hdelay_removed(i,j,view_data_range))), imag(squeeze(Hdelay_removed(i,j,view_data_range))), 'k'); grid on; hold on;
    xlabel('real [m/N]'); ylabel('imag [m/N]'); title(['Real & Imaginary Components G' num2str(i) num2str(j)]);
    legend('Dynamics with assumed delay removed','Location','Best');
%     axis([-mag_view_max mag_view_max -mag_view_max mag_view_max]);
    k = k + 1; 
        end
    end
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
end


